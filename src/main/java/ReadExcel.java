import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.TEAF.framework.GetPageObjectRead;

import junit.framework.Assert;

public class ReadExcel {
	
	public static String fetchValuesFromSpreadSheet(String sheetname, String columnname) throws Exception {
		String cellValue = null;
		
		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);

			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				String stringCellValue = sheet.getRow(0).getCell(i).getStringCellValue();
				System.out.println("Header " + stringCellValue);
				if (stringCellValue.equalsIgnoreCase(columnname)) {
					Row row = sheet.getRow(1);

					Cell cell = row.getCell(i);
					if (cell == null) {
						continue;
					}
					CellType cellType = cell.getCellType();
					if (cellType.equals(CellType.STRING)) {
						cellValue = row.getCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
					} else if (cellType.equals(CellType.NUMERIC)) {
						double numericCellValue = row.getCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK)
								.getNumericCellValue();
						long l = (long) numericCellValue;
						cellValue = String.valueOf(l);
					}
					System.out.println("Valuesss " + cellValue);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// throw new Exception();
		}
		return cellValue;

	}
	
	public static void updateValuesToExcel(String sheetname, String columnname, String value, int rowNum)
			throws Exception {
		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row row = sheet.getRow(0);
			for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {

				Cell cell = row.getCell(i);
				String stringCellValue = cell.getStringCellValue();
				if (stringCellValue.equals(columnname)) {
					Row data = sheet.getRow(rowNum);
					if (data == null) {
						data = sheet.createRow(rowNum);
					}

					Cell c1 = data.getCell(i);
					if (c1 == null) {
						c1 = data.createCell(i);
					}
					c1.setCellValue(value);
				}
			}
			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// throw new Exception();
		} //

	}
	
	public static void ReadExcelValueIsContained(String sheetname) {
		
		try {
			java.io.File f = new java.io.File(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue="";
			String TotalValue="";
			String Total="";
			
			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
					
				if(hr.getCell(i).getStringCellValue().equals("TotalPrice")) {
					cellValue = content.getCell(i).getStringCellValue().toString();
					TotalValue = StringUtils.stripStart(cellValue, "$");
					Total="USD "+TotalValue;
				}
			}
			System.out.println(Total);	
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static void main(String[] args) {
		ReadExcelValueIsContained("Quotes");
	}

}
