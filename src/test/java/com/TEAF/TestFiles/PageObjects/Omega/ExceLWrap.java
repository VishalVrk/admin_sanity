package com.TEAF.TestFiles.PageObjects.Omega;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.print.DocFlavor.STRING;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExceLWrap {

	public static void main(String[] args) throws IOException {
		
		Set<String> s = new LinkedHashSet<String>();
		
		List<String> readAllLines = Files.readAllLines(Paths.get("E:\\Omega_AutomationRMA\\qa_omegasmoke\\src\\test\\java\\com\\TEAF\\TestFiles\\PageObjects\\Omega\\Omega_RMA.feature"));
		for (String x : readAllLines) {
				if (x.contains("'")) {
					String[] split = x.split("'");
					System.out.println(split[1]);
					s.add(split[1]);
				}
		}
		
		List<String> readAllLines1 = Files.readAllLines(Paths.get("E:\\Omega_AutomationRMA\\qa_omegasmoke\\src\\test\\java\\com\\TEAF\\TestFiles\\PageObjects\\Omega\\Omega_Smoke.feature"));
		for (String x : readAllLines1) {
				if (x.contains("'")) {
					String[] split = x.split("'");
					System.out.println(split[1]);
					s.add(split[1]);

				}
		}
		System.out.println(s.size()+"= count");
		Map<String, String> mp = new HashMap<String, String>();
		File f = new File("E:\\Omega_AutomationRMA\\qa_omegasmoke\\src\\test\\java\\com\\TEAF\\TestFiles\\PageObjects\\Omega");
		File[] listFiles = f.listFiles();
		for (int i = 0; i < listFiles.length; i++) {
			if (listFiles[i].getAbsoluteFile().toString().endsWith(".xlsx")) {
				File file = listFiles[i];
				
				FileInputStream fi = new FileInputStream(file);
				Workbook wb = new XSSFWorkbook(fi);
				Sheet sheetAt = wb.getSheetAt(0);
				for (int j = 0; j < sheetAt.getPhysicalNumberOfRows(); j++) {
					Row row = sheetAt.getRow(j);
					Cell cell = row.getCell(0);
					String stringCellValue = cell.getStringCellValue();
					if (s.contains(stringCellValue)) {
						String stringCellValue3 = row.getCell(1).getStringCellValue();

						String stringCellValue2 = row.getCell(2).getStringCellValue();
						mp.put(stringCellValue,stringCellValue3+"~"+stringCellValue2);
						
					}
					
				}
			}
		}	
		
		File wf = new File("E:\\Omega_AutomationRMA\\qa_omegasmoke\\src\\test\\java\\com\\TEAF\\TestFiles\\PageObjects\\Omega\\NewFile.xlsx");
		FileInputStream fin = new FileInputStream(wf);
		Workbook wb = new XSSFWorkbook(fin);
		Sheet sheetAt = wb.getSheetAt(0);
		Set<Entry<String,String>> entrySet = mp.entrySet();
		int i = 1;
		for (Entry<String, String> entry : entrySet) {
			sheetAt.createRow(i).createCell(0).setCellValue(entry.getKey());
			System.out.println(entry.getKey());
			String value = entry.getValue();
			String[] split = value.split("~");
			System.out.println(entry.getValue());
			sheetAt.getRow(i).createCell(1).setCellValue(split[0]);
			sheetAt.getRow(i).createCell(2).setCellValue(split[1]);
			i++;
			System.out.println(i);

		}
		FileOutputStream fout = new FileOutputStream(wf);
		wb.write(fout);
		wb.close();
		
	
	}
}
