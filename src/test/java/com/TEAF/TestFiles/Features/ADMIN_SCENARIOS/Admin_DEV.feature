@Admin_RUN_DEV
Feature: End to End scenario for Global search , Quick order ,Add to cart , Credit Card Payment, Account Payment

@Iniital
Scenario: TC-1.0:Validate Home Page Contents
Given My WebApp 'Omega' is open
Then I should see element 'OmegaLogo' present on page
Then I should see element 'AllProducts_Menu' present on page
Then I should see element 'ResouRces_Menu' present on page
Then I should see element 'SignIn_Btn' present on page
Then I should see element 'Register_1' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrder_Text' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrderQuote' present on page
Then I should see element 'WhatsNewSection' present on page

@CreateQuote
Scenario: TC-2.0: Verify Prepay user is able to sign successfully
And I refresh the WebPage
Given My WebApp 'Omega' is open
When I click 'OmegaLogo'
And I click 'SignIn_Btn'
Then I should see element 'SignID_Field' present on page
Then I should see element 'Password_Field' present on page
Then I should see element 'LogIn_Btn' present on page
Then I clear field 'SignID_Field'
And I enter 'taha.patel_pp_us@royalcyber.com' in field 'SignID_Field'
And I clear field 'Password_Field'
And I enter 'Omega2003@' in field 'Password_Field'  
And I click 'LogIn_Btn'
Then I should see text 'Welcome Taha Patel' present on page at 'Welcome'
And I remove items from cart

@CreateQuote
Scenario: TC-3.0:Verify Quote functionality Quote Creation
When I click 'OmegaLogo'
When I enter 'HTC-030' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'List_Table_button'
And I click 'Product'
And I wait for '5' seconds 	
And I click 'AddToCart'
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'OmegaLogo'
And I wait for '5' seconds
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I focus and click 'Checkout_MiniCart'
Then I should see element 'Quote_Btn' present on page
And I click 'Quote_Btn'
Then I should see text 'This cart is being quoted' present on page at 'Quote_Title'
Then I get the value from Support Ticket page 'Quote_Name' and store as 'QuoteName'
And I wait for '10' seconds
And I click 'SubmitQuote_Btn'
Then I should see element 'Quote_Popup' present on page
Then I should see element 'Popup_Title' present on page
Then I should see element 'SubmitQuote_Yes_Btn' present on page
And I wait for '5' seconds
And I click 'SubmitQuote_Yes_Btn'
Then I should see element 'QuoteSuccess_Msg' present on page
And I click 'CreatedQuoteID'
Then I should see text 'Quote Details' present on page at 'QuoteDetailTitle'
And I fetch Quote values and store in excel sheet 'Quotes' : Sheet

Scenario: TC-4.0: Verify the Corporate user is able to sign-out
And I refresh the WebPage
And I click 'OmegaLogo'
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I click 'Sign_Out'
Then I should see element 'SignIn_Btn' present on page

Scenario: TC-5.0:Verify Sign-in feature as Corporate User
When I click 'OmegaLogo'
And I click 'SignIn_Btn'
Then I should see element 'SignID_Field' present on page
Then I should see element 'Password_Field' present on page
Then I should see element 'LogIn_Btn' present on page
Then I clear field 'SignID_Field'
And I enter 'taha.patel_ap_us@royalcyber.com' in field 'SignID_Field'
And I clear field 'Password_Field'
And I enter 'Omega2003@' in field 'Password_Field'  
And I click 'LogIn_Btn'
Then I should see text 'Welcome Test Patel' present on page at 'Welcome'
And I remove items from cart

@tesstt
Scenario: TC-6.0:Verify product search functionality - Global Search
And I refresh the WebPage
When I click 'OmegaLogo'
When I enter 'HTC-030' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'List_Table_button'
And I click 'Product'
And I wait for '2' seconds
And I click 'AddToCart'
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
And I should see element 'CartPageHeading' present on page_

@tesstt
Scenario Outline: TC-7.0:Verify place order functionality for Corporate customer using Account Payment method
Then I should see element 'Checkout' present on page_
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
Then I click 'AccountPaymentType'
Then I should see element 'CostCenter' present on page
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I should see element 'Customer_number' present on page
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I scroll to 'coordinates' - '0,-200'
When I click 'Next_CartPage'
And I wait for '5' seconds
When I click 'SubmitAsIs'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I click shipping dropdown button if displayed
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I wait for '5' seconds
And I should see element 'Success_Header' present on page
And I should see element 'OrderNumber' present on page_
And I fetch values and store in excel sheet 'AccountPayment' : Sheet


Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

Scenario: TC-8.0: Verify the Corporate user is able to sign-out
And I refresh the WebPage
And I click 'OmegaLogo'
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I click 'Sign_Out'
Then I should see element 'SignIn_Btn' present on page

@CPQ_OrderDEV
Scenario: TC-9.0:Verify Sign-in Web customer
And I wait for '5' seconds
And I click 'SignIn_Btn'
Then I should see element 'SignID_Field' present on page
Then I should see element 'Password_Field' present on page
Then I should see element 'LogIn_Btn' present on page
Then I clear field 'SignID_Field'
And I enter 'taha.patel_c_us@royalcyber.com' in field 'SignID_Field'
And I clear field 'Password_Field'
And I enter 'Omega2003@' in field 'Password_Field'
And I click 'LogIn_Btn'
Then I should see text 'Welcome Taha Patel' present on page at 'Welcome'
And I remove items from cart

@Admin_08
Scenario: TC-10.0:Verify product search functionality - Quick Order Search through 2 ways
And I wait for '5' seconds
And I click 'OmegaLogo'
Then I should see element 'QuickOrder_Header' present on page
And I click 'QuickOrder_Header' 
Then I should see element 'SKU_field' present on page
When I enter 'T-FER-1/16' in field 'SKU_field'
And I press enter key
And I clear field 'QuickOrderQuoteUpdateQty'
And I clear the text and enter '2' in field 'QuickOrderQuoteUpdateQty'
And I press key: enter
And I scroll to 'coordinates' - '0,-250'
And I click 'QuickOrderQuote_AddtoCart'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
And I wait for '3' seconds
When I click 'OmegaLogo'
Then I should see element 'QuickOrder_Partnumfield1' present on page
And I wait for '3' seconds
When I enter 'HTC-030' in field 'QuickOrder_Partnumfield1'
And I wait for '3' seconds
And I get text from 'QuickOrder_Partnumfield1' and store
And I click 'QuickOrder_Qtyfield1'
And I enter '1' in field 'QuickOrder_Qtyfield1'
And I click 'QuickOrder_Add2CartBtn'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'

@Admin_08
Scenario Outline:TC-11.0: Verify place order functionality for Credit Card Payment method
And I wait for '5' seconds
And I refresh the WebPage
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,-150'
When I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '4' seconds
And I focus and click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page_
When I click 'Checkout'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
#And I enter '<FN_Cart>' in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I enter 'Test' in field 'DeliveryAttention'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
#add one step
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '15' seconds
And I click shipping dropdown button if displayed
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'Success_Header' present on page
And I should see element 'OrderNumber' present on page_
And I fetch values and store in excel sheet 'WebCustomerCC' : Sheet multi-line

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

@CPQ_OrderDEV
Scenario: TC-12.0:Verify configure functionality Creation of configure product
And I wait for '5' seconds
And I refresh the WebPage
When I click 'OmegaLogo'
#And I remove items from cart
And I wait for '2' seconds
When I enter 'sa1e' in field 'TextBox_Search'
And I click 'Search_Icon'
And I press key: enter
And I wait for '2' seconds
And I click 'List_Table_button'
And I should see element 'ProductTypeElement' present on page
And I click 'ProductTypeElement'
And I wait for '2' seconds
And I click 'ViewDetailsLink'
And I wait for '2' seconds
And I click 'Customize_Button'
And I switch to iFrame Configure Product
And I wait '5' seconds for presence of element 'JType'
Then I wait for '5' seconds
And I should see element 'JType' present on page
And I click 'JType'
And I wait '5' seconds for presence of element '72InElement'
And I should see element '72InElement' present on page
Then I wait for '5' seconds
And I click '72InElement'
And I wait '5' seconds for presence of element 'NextButton_CPQ'
And I should see element 'NextButton_CPQ' present on page
Then I wait for '5' seconds
And I click 'NextButton_CPQ'
And I wait '5' seconds for presence of element 'M8_plug'
And I should see element 'M8_plug' present on page
And I click 'M8_plug'
And I wait '5' seconds for presence of element 'NextButton_CPQ'
And I should see element 'NextButton_CPQ' present on page
Then I wait for '5' seconds
And I click 'NextButton_CPQ'
And I wait '5' seconds for presence of element 'CPQ_None'
And I should see element 'CPQ_None' present on page
And I click 'CPQ_None'
And I wait for '2' seconds
And I wait '5' seconds for presence of element 'NextButton_CPQ'
And I should see element 'NextButton_CPQ' present on page
Then I wait for '5' seconds
And I click 'NextButton_CPQ'
And I wait '5' seconds for presence of element 'Save_Config'
And I should see element 'Save_Config' present on page
And I click 'Save_Config'
And I switch to back to default content
And I wait for '2' seconds
Then I should see element 'SavedConfigurationHeader' present on page
And I wait for '2' seconds
When I click 'ConfigurePopup_AddtoCart'
And I wait for '2' seconds
And I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I wait for element present 'QuickOrder_ContinueShopping'
And I click 'QuickOrder_ContinueShopping'

@CPQ_OrderDEV
Scenario Outline: TC-13.0: Placing order of CPQ product
And I wait for '5' seconds
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '3' seconds
And I focus and click 'Checkout_MiniCart'
And I wait for '2' seconds
Then I should see element 'Checkout' present on page_
And I click 'Checkout'
Then I should see element 'CardPayment' present on page
And I click 'CardPayment'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I wait for '5' seconds
And I enter 'Test' in field 'DeliveryAttention'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I click shipping dropdown button if displayed
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'OrderNumber' present on page_
And I should see element 'Success_Header' present on page
And I should see element 'OrderNumber' present on page_

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

Scenario: TC-14.0: Verify if the user is navigated to ASM applicaiton and validate
And I remove items from cart 
And I wait for '5' seconds
When I click 'OmegaLogo'
Then I navigate to asm application
Then I should see element 'OmegaLogo' present on page
And I wait for '5' seconds
Then I should see element 'AllProducts_Menu' present on page
Then I should see element 'ResouRces_Menu' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrder_Text' present on page


Scenario: TC-15.0: Verify if the user is able to sign-in into ASM agent
And I wait for '5' seconds
Then I should see element 'ASM_Logo' present on page_
Then I should see element 'Asagent_username' present on page
Then I should see element 'Asagent_password' present on page
Then I clear field 'Asagent_username'
When I enter 'tpatel@omega.com' in field 'Asagent_username'
And I clear field 'Asagent_password'
And I enter '123456' in field 'Asagent_password'
And I click 'Asagent_SignIn'

Scenario: TC-16.0: Placing Order using Quick View
And I wait for '5' seconds
And I refresh the WebPage
When I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I wait for '5' seconds
#And I click 'List_Table_button'
And I scroll to 'coordinates' - '0,750'
And I click 'trT-FER-1-16'
And I should see element 'VolumeTableBox' present on page
And I clear field 'qtyInputTable'
And I enter '1' in field 'qtyInputTable'
And I click 'TableAddtoCartButton'
And I wait for '5' seconds
When I click 'CheckoutBtnCartPopUp'
Then I should see element 'Checkout' present on page_
And I click 'Checkout'

Scenario Outline:TC-17.0: Verify ASM place order functionality
And I wait for '5' seconds
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
#And I enter '<FN_Cart>' in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I enter 'Test' in field 'DeliveryAttention'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '15' seconds
And I click shipping dropdown button if displayed
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'Success_Header' present on page
And I should see element 'OrderNumber' present on page_

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

Scenario: TC-18.0: Verify Web user is able to sign-out successfully
And I refresh the WebPage
And I click 'OmegaLogo'
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I click 'Sign_Out'
Then I should see element 'SignIn_Btn' present on page