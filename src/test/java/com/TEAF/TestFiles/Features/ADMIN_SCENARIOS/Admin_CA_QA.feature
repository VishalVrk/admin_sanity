@Admin_RUN_CAQA
Feature: End to End scenario for Global search , Quick order ,Add to cart , Credit Card Payment, Account Payment

Scenario: TC-1.0:Validate Home Page Contents
Given My WebApp 'Omega' is open
Then I should see element 'OmegaLogo' present on page
Then I should see element 'AllProducts_Menu' present on page
Then I should see element 'ResouRces_Menu' present on page
Then I should see element 'SignIn_Btn' present on page
Then I should see element 'Register_1' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrder_Text' present on page
Then I should see element 'Banner' present on page_
Then I should see element 'QuickOrderQuote' present on page
Then I should see element 'WhatsNewSection' present on page 

Scenario: TC-2.0:Verify Sign-in feature as Corporate User
When I click 'OmegaLogo'
And I click 'SignIn_Btn'
Then I should see element 'SignID_Field' present on page
Then I should see element 'Password_Field' present on page
Then I should see element 'LogIn_Btn' present on page
Then I clear field 'SignID_Field'
And I enter 'logeshkumar.4567@royalcyber.com' in field 'SignID_Field'
And I clear field 'Password_Field'
And I enter 'Omega2003@' in field 'Password_Field'  
And I click 'LogIn_Btn' 
Then I should see text 'Welcome Logesh kumar' present on page at 'Welcome'
And I remove items from cart

Scenario: TC-3.0:Verify product search functionality - Global Search
When I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I wait for '3' seconds
And I click 'List_Table_button'
And I wait for '3' seconds
And I click 'Product'
And I click 'AddToCart'
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'


Scenario Outline: TC-4.0:Verify place order functionality for Corporate customer using Account Payment method
Then I should see element 'Checkout' present on page_
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
Then I click 'AccountPaymentType'
Then I should see element 'CostCenter' present on page_
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I should see element 'Customer_number' present on page
And I click 'Next_CartPage'
And I select option '1' in dropdown 'delivery-country' by 'index'
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option '1' in dropdown 'StateDD_Cart' by 'index'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,-200'
When I click 'Next_CartPage'
And I wait for '5' seconds
When I click 'SubmitAsIs'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I click shipping dropdown button if displayed
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'Success_Header' present on page
And I should see element 'OrderNumber' present on page_
Examples:
|LN_Cart  |CN_Cart        |AddL1	      |Town_City	    |Pin	    |Phone	         |
|Test	  |Account Payment|4937 Yonge St  |Toronto Ontario	|M2N 5N6    |705-750-8910	 |

Scenario: TC-5.0: Verify the Webuser user is able to sign-out
And I refresh the WebPage
And I click 'OmegaLogo'
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I click 'Sign_Out'
Then I should see element 'SignIn_Btn' present on page

Scenario: TC-6.0: Verify Prepay user is able to sign successfully
Given My WebApp 'Omega' is open
When I click 'OmegaLogo'
And I click 'SignIn_Btn'
Then I should see element 'SignID_Field' present on page
Then I should see element 'Password_Field' present on page
Then I should see element 'LogIn_Btn' present on page
Then I clear field 'SignID_Field'
And I enter 'taha.patel_pp_ca@royalcyber.com' in field 'SignID_Field'
And I clear field 'Password_Field'
And I enter 'Omega2003@' in field 'Password_Field'  
And I click 'LogIn_Btn'
Then I should see text 'Welcome Taha Patel' present on page at 'Welcome'
And I remove items from cart

Scenario Outline:TC-7.0: Verify place order functionality in prepay account
When I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I wait for '3' seconds
And I click 'List_Table_button'
And I wait for '3' seconds
And I click 'Product'
And I click 'AddToCart'
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'

Then I should see element 'Checkout' present on page_
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
Then I click 'PrepayOption'

And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I should see element 'Customer_number' present on page
And I click 'Next_CartPage'
And I select option '1' in dropdown 'delivery-country' by 'index'
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option '1' in dropdown 'StateDD_Cart' by 'index'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,-200'
When I click 'Next_CartPage'
And I wait for '5' seconds
When I click 'SubmitAsIs'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I click shipping dropdown button if displayed
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'Success_Header' present on page
And I should see element 'OrderNumber' present on page_

And I refresh the WebPage
And I click 'OmegaLogo'
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I click 'Sign_Out'
Then I should see element 'SignIn_Btn' present on page
Examples:
|LN_Cart  |CN_Cart        |AddL1	      |Town_City	    |Pin	    |Phone	         |
|Test	  |Prepay         |4937 Yonge St  |Toronto Ontario	|M2N 5N6    |705-750-8910	 |

@Admin_13
Scenario: TC-8.0:Verify Sign-in Web customer
Given My WebApp 'Omega' is open
And I click 'SignIn_Btn'
Then I should see element 'SignID_Field' present on page
Then I should see element 'Password_Field' present on page
Then I should see element 'LogIn_Btn' present on page
Then I clear field 'SignID_Field'
And I enter 'taha.patel_wc_ca@royalcyber.com' in field 'SignID_Field'
And I clear field 'Password_Field'
And I enter 'Omega2003@' in field 'Password_Field'
And I click 'LogIn_Btn'
Then I should see text 'Welcome Taha Patel' present on page at 'Welcome'
And I remove items from cart 

Scenario: TC-9.0:Verify product search functionality - Quick Order Search through 2 ways
And I click 'OmegaLogo'
Then I should see element 'QuickOrder_Header' present on page
And I click 'QuickOrder_Header' 
Then I should see element 'SKU_field' present on page
When I enter 'HTC-030' in field 'SKU_field'
And I press enter key
And I clear field 'QuickOrderQuoteUpdateQty'
And I clear the text and enter '2' in field 'QuickOrderQuoteUpdateQty'
And I press enter key
And I click 'QuickOrderQuote_AddtoCart'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
And I wait for '3' seconds
When I click 'OmegaLogo'
Then I should see element 'QuickOrder_Partnumfield1' present on page
And I wait for '3' seconds
When I enter '4001ajc' in field 'QuickOrder_Partnumfield1'
And I wait for '3' seconds
And I get text from 'QuickOrder_Partnumfield1' and store
And I click 'QuickOrder_Qtyfield1'
And I enter '1' in field 'QuickOrder_Qtyfield1'
And I click 'QuickOrder_Add2CartBtn'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'

Scenario Outline:TC-10.0: Verify place order functionality for Credit Card Payment method
And I wait for '5' seconds
And I refresh the WebPage
And I wait for '2' seconds
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '4' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page_
When I click 'Checkout'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select option '1' in dropdown 'delivery-country' by 'index'
#And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
#And I enter '<FN_Cart>' in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option '1' in dropdown 'StateDD_Cart' by 'index'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I enter 'Test' in field 'DeliveryAttention'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter 'M2N 5N6' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '15' seconds
And I click shipping dropdown button if displayed
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'Success_Header' present on page
And I should see element 'OrderNumber' present on page_
Examples:
|LN_Cart  |CN_Cart        |AddL1	      |Town_City	    |Pin	    |Phone	         |
|Test	  |Account Payment|4937 Yonge St  |Toronto Ontario	|M2N 5N6    |705-750-8910	 |

Scenario: TC-11.0:Verify configure functionality Creation of configure product
And I wait for '3' seconds
When I click 'OmegaLogo'
And I remove items from cart
When I enter 'sa1e' in field 'TextBox_Search'
And I click 'Search_Icon'
And I wait for '3' seconds
And I click 'List_Table_button'
And I wait for '3' seconds
And I click 'Product'
And I click 'Customize_Button'
And I switch to iFrame Configure Product
And I wait for '2' seconds
And I wait '5' seconds for presence of element 'JType'
And I should see element 'JType' present on page
Then I wait for '5' seconds
And I click 'JType'
And I wait '5' seconds for presence of element '72InElement'
And I should see element '72InElement' present on page
Then I wait for '5' seconds
And I click '72InElement'
And I wait '5' seconds for presence of element 'NextButton_CPQ'
And I should see element 'NextButton_CPQ' present on page
Then I wait for '5' seconds
And I click 'NextButton_CPQ'
And I wait '5' seconds for presence of element 'M8_plug'
And I should see element 'M8_plug' present on page
And I click 'M8_plug'
And I wait '5' seconds for presence of element 'NextButton_CPQ'
And I should see element 'NextButton_CPQ' present on page
Then I wait for '5' seconds
And I click 'NextButton_CPQ'
And I wait '5' seconds for presence of element 'CPQ_None'
And I should see element 'CPQ_None' present on page
And I click 'CPQ_None'
And I wait for '2' seconds
And I wait '5' seconds for presence of element 'NextButton_CPQ'
And I should see element 'NextButton_CPQ' present on page
Then I wait for '5' seconds
And I click 'NextButton_CPQ'
And I wait '5' seconds for presence of element 'Save_Config'
And I should see element 'Save_Config' present on page
And I click 'Save_Config'
And I switch to back to default content
And I wait for '2' seconds
Then I should see element 'SavedConfigurationHeader' present on page
And I wait for '2' seconds
When I click 'ConfigurePopup_AddtoCart'
And I wait for '2' seconds
And I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I wait for element present 'QuickOrder_ContinueShopping'
And I click 'QuickOrder_ContinueShopping'

Scenario Outline: TC-12.0: Placing order of CPQ product
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '3' seconds
And I click 'Checkout_MiniCart'
And I wait for '2' seconds
Then I should see element 'Checkout' present on page_
And I click 'Checkout'
Then I should see element 'CardPayment' present on page
And I click 'CardPayment'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select option '1' in dropdown 'delivery-country' by 'index'
And I enter name in field 'FirstName_Cart'
#And I enter '<FN_Cart>' in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I select option '1' in dropdown 'StateDD_Cart' by 'index'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I wait for '5' seconds
And I enter 'Test' in field 'DeliveryAttention'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter 'M2N 5N6' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I click shipping dropdown button if displayed
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'Success_Header' present on page
And I should see element 'OrderNumber' present on page_

Examples:
|LN_Cart  |CN_Cart        |AddL1	      |Town_City	    |Pin	    |Phone	         |
|Test	  |Card Payment   |4937 Yonge St  |Toronto Ontario	|M2N 5N6    |705-750-8910	 |

@Admin_13
Scenario: TC-13.0: Verify if the user is navigated to ASM applicaiton and validate
And I remove items from cart
When I click 'OmegaLogo'
Then I navigate to asm application
Then I should see element 'OmegaLogo' present on page
And I wait for '5' seconds
Then I should see element 'AllProducts_Menu' present on page
Then I should see element 'ResouRces_Menu' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrder_Text' present on page_

@Admin_13
Scenario: TC-14.0: Verify if the user is able to sign-in into ASM agent
Then I should see element 'ASM_Logo' present on page_
Then I should see element 'Asagent_username' present on page
Then I should see element 'Asagent_password' present on page
Then I clear field 'Asagent_username'
When I enter 'asagent' in field 'Asagent_username'
And I clear field 'Asagent_password'
And I enter '123456' in field 'Asagent_password'
And I click 'Asagent_SignIn'

@Admin_13
Scenario: TC-15.0: Placing Order using Quick View
When I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I scroll to 'coordinates' - '0,750'
And I wait for '2' seconds
And I focus and click 'trT-FER-1-16'
And I should see element 'VolumeTableBox' present on page
And I clear field 'qtyInputTable'
And I enter '1' in field 'qtyInputTable'
And I click 'TableAddtoCartButton'
And I wait for '5' seconds
When I click 'CheckoutBtnCartPopUp'
Then I should see element 'Checkout' present on page_
And I click 'Checkout'

@Admin_13
Scenario Outline:TC-16.0: Verify ASM place order functionality
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
#And I select US from dropdown 
And I select option '1' in dropdown 'delivery-country' by 'index'
And I enter name in field 'FirstName_Cart'
#And I enter '<FN_Cart>' in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I select option '1' in dropdown 'StateDD_Cart' by 'index'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I enter 'Test' in field 'DeliveryAttention'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter 'M2N 5N6' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '15' seconds
And I click shipping dropdown button if displayed
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'Success_Header' present on page
And I should see element 'OrderNumber' present on page_

Examples:
|LN_Cart  |CN_Cart        |AddL1	      |Town_City	    |Pin	    |Phone	         |
|Test	  |Card Payment   |4937 Yonge St  |Toronto Ontario	|M2N 5N6    |705-750-8910	 |


