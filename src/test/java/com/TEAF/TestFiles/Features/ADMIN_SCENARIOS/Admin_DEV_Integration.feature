@Admin_DEV_Integration
Feature: Integration System Testing for Salesforce

Scenario: Waiting for 15 minutes to reflect in integration systems
And I load the excel sheet data to validate the Syteline
Given I wait for '1500' seconds 

Scenario: TC-1.0:Verify place order functionality for Corporate customer using Account Payment in Salesforce
Given My WebApp 'Omega' is open
And I Login Salesforce Application
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I wait for '5' seconds
And I enter 'OrderNumber' from 'AccountPayment' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents in salesforce from search page from 'AccountPayment' : sheet
And I click 'SalesforceOrderStatusNumberLink'
And I wait for '2' seconds
And I refresh the WebPage
And I validate header for orders contents in salesforce in 'AccountPayment' : sheet
And I validate salesforce details from 'AccountPayment' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate order single line items from 'AccountPayment' :sheet

Scenario: TC_02: Verify place order functionality for Credit Card Payment method in Salesforce Multi-line
Then I click 'SalesforceOmegaLogo' 
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'WebCustomerCC' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents in salesforce from search page from 'WebCustomerCC' : sheet
And I click 'SalesforceOrderStatusNumberLink'
And I wait for '2' seconds
And I refresh the WebPage
And I validate header for orders contents in salesforce in 'WebCustomerCC' : sheet
And I validate salesforce details from 'WebCustomerCC' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate order multi line items from 'WebCustomerCC' :sheet in reverse

Scenario: TC_03:Verify Quote functionality Quote Creation
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I wait for '5' seconds
And I clear field 'SalesforceSearch'
And I enter 'QuoteNumber' from 'Quotes' sheet to the field 'SalesforceSearch'
And I wait for '2' seconds
And I press key: enter
And I press key: enter
And I validate contents in Quote salesforce from search page from 'Quotes' : sheet
And I click 'SalesforceQuoteNumberLink'
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '2' seconds
And I validate Quote salesforce from Header page from 'Quotes' : sheet
And I validate ADMIN details from sheet 'Quotes'
And I validate quote information details from sheet 'Quotes'
And I validate quote Totals details from sheet 'Quotes'
And I validate quote Prepared for details from sheet 'Quotes'
And I validate order single quote line items from 'Quotes' :sheet

@sytline
Scenario: TC_04:Verify Order reaching Sytline for Account Payment method
And I Login into Sytline DEV
And I open form and search 'Customer Orders' & select 'CustomerOrders_SL' then I should see 'OrderNumber_SL'
And I wait for '10' seconds
And I enter 'OrderNumber' from 'AccountPayment' and filter the order
And I should see the order details 'AccountPayment' feilds present in the page
And I click on Contacts and verify the contact details 'AccountPayment' present in the page
And I click 'LinesButton_SL'
And I wait for '30' seconds
And I should see order line item 1 and corresponding details 'AccountPayment' present in the page

@sytline
Scenario: TC_05:Verify Order reaching Sytline for Web Payment method
And I Login into Sytline DEV
And I open form and search 'Customer Orders' & select 'CustomerOrders_SL' then I should see 'OrderNumber_SL'
And I wait for '10' seconds
And I enter 'OrderNumber' from 'WebCustomerCC' and filter the order
And I should see the order details 'WebCustomerCC' feilds present in the page
And I click on Contacts and verify the contact details 'WebCustomerCC' present in the page
And I click 'LinesButton_SL'
And I wait for '30' seconds
And I should see order line item 1 and corresponding details 'WebCustomerCC' present in the page
And I click item 2 with order number sheet 'WebCustomerCC' and column 'OrderNumber'
And I should see order line item 2 and corresponding details 'WebCustomerCC' present in the page