package com.TEAF.stepDefinitions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tools.ant.filters.TokenFilter.ContainsString;
import org.apache.tools.ant.taskdefs.Truncate;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;
import com.TEAF.stepDefinitions.CommonSteps;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.javascript.host.file.File;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.lu.an;
import cucumber.runtime.CucumberException;

public class Omega_CustomMethods {

	static WrapperFunctions wrapFunc = new WrapperFunctions();
	static StepBase sb = new StepBase();
	// static StepBase_D2 sb = new StepBase_D2();
	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();

	@Given("^I verify title of the page$")
	public static void I_verify_title_of_the_page() throws Exception {
		try {
			String actualTitle = driver.getTitle();
			System.out.println(actualTitle);
			String expectedTitle = "About Us | Omega Engineering";
			Assert.assertEquals(expectedTitle, actualTitle);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}
	
	@Given("^I get 60 days before given date and enter at '(.*)'$")
	public static void i_get_60_days_before_given_date(String locator) throws Exception {
		WebElement element =driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		String value =element.getAttribute("value");
		SimpleDateFormat formatter=new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");  
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -60);
		String finalDate = formatter.format(cal.getTime());
		System.out.println("Final Date: "+finalDate);
		element.sendKeys(finalDate);
	}
	
	
	@Given("^I validate restocking fee validation '(.*)' :Price '(.*)' :Qty '(.*)' :RestockingFee '(.*)' :Total$")
	public static void i_validate_restocking_fee_validation(String price,String Quantity,String restockAmount, String Total) throws Exception {
		WebElement PriceElement = driver.findElement(GetPageObjectRead.OR_GetElement(price));
		wrapFunc.highLightElement(PriceElement);
		WebElement QuantityElement = driver.findElement(GetPageObjectRead.OR_GetElement(Quantity));
		wrapFunc.highLightElement(QuantityElement);
		WebElement restockAmountElement = driver.findElement(GetPageObjectRead.OR_GetElement(restockAmount));
		wrapFunc.highLightElement(restockAmountElement);
		WebElement TotalElement = driver.findElement(GetPageObjectRead.OR_GetElement(Total));
		wrapFunc.highLightElement(TotalElement);
		String PriceValue = PriceElement.getText().trim();
		PriceValue=StringUtils.stripStart(PriceValue, "$");
		System.out.println(PriceValue);
		String QuantityValue = QuantityElement.getText().trim();
		System.out.println(QuantityValue);
		String restockAmountValue = restockAmountElement.getText().trim();
		restockAmountValue=StringUtils.stripStart(restockAmountValue, "$");
		System.out.println(restockAmountValue);
		String TotalValue = TotalElement.getText().trim();
		TotalValue=StringUtils.stripStart(TotalValue, "$");
		System.out.println(TotalValue);
		
//		int priceInt = Integer.parseInt(PriceValue);
		double priceInt = Double.parseDouble(PriceValue);
		double QtyInt = Double.parseDouble(QuantityValue); 
		double rstkAmtInt = Double.parseDouble(restockAmountValue);
		double totalInt= Double.parseDouble(TotalValue);		
		rstkAmtInt = priceInt*0.2*QtyInt;
		totalInt = (priceInt*QtyInt)-rstkAmtInt;
		String RestockFeeFinal = new DecimalFormat("0.00").format(rstkAmtInt);
		System.out.println(RestockFeeFinal);
		String TotalAmountFinal =new DecimalFormat("0.00").format(totalInt);
		System.out.println(TotalAmountFinal);
		Assert.assertEquals(restockAmountValue, RestockFeeFinal);
		wrapFunc.highLightElement(restockAmountElement);
		Assert.assertEquals(TotalValue, TotalAmountFinal);
		wrapFunc.highLightElement(TotalElement);
		
	}
	
	
	@Given("^I should see '(.*)' from field$")
	public void i_should_see_text_from_input_field(String locator) {
		WebElement element =driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		String value =element.getAttribute("value");
		value = value.trim();
		System.out.println("Value from Text area: "+value);
	}

	@Given("^I enter text '(.*)' in textarea at '(.*)' field$")
	public static void I_Click_TAB(String text, String locator) throws Exception {
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		Thread.sleep(2000);
		element.sendKeys(Keys.TAB);
		element.clear();
		element.sendKeys(text);
	}

	@Given("^I Zoom out$")
	public static void I_Zoom_out() {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("document.body.style.zoom = '0.75'");
	}

	public static String fetchValuesFromSpreadSheet(String sheetname, String columnname) throws Exception {
		StringBuffer cellValue = new StringBuffer();
		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);

			int rowCount = sheet.getPhysicalNumberOfRows();

			for (int r = 0; r < rowCount; r++) {

				Row hr = sheet.getRow(0);

				for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
					String stringCellValue = sheet.getRow(0).getCell(i).getStringCellValue();
					System.out.println("Header " + stringCellValue);
					if (stringCellValue.equalsIgnoreCase(columnname)) {
						Row row = sheet.getRow(r + 1);
						if (row == null) {
							continue;
						}
						Cell cell = row.getCell(i);
						if (cell == null) {
							continue;
						}
						CellType cellType = cell.getCellType();
						if (cellType.equals(CellType.STRING)) {
							String cv = row.getCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
							cellValue.append(cv + "&");

						} else if (cellType.equals(CellType.NUMERIC)) {
							double numericCellValue = row.getCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK)
									.getNumericCellValue();
							long l = (long) numericCellValue;
							String cv = String.valueOf(l);
							cellValue.append(cv + "&");

						}
					}
				}
			}
			String itemValue = cellValue.toString();
			int andCount = 0;
			for (int i = 0; i < itemValue.length(); i++) {
				if (itemValue.charAt(i) == '&') {
					andCount = andCount + 1;
				}
			}
			if (andCount == 1) {
				itemValue = itemValue.replace("&", "");
			}
			return itemValue;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}

	}

	@When("I should see item name '(.*)' : '(.*)' present on the shipping page$")
	public static void i_should_See_item_name_present_on_the_shipping_page(String sheetName, String columnName)
			throws Exception {

		String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetName, columnName);
		if (fetchValuesFromSpreadSheet.contains("&")) {
			String[] split;
			split = fetchValuesFromSpreadSheet.split("&");
			for (int i = 0; i < split.length; i++) {
				String itemfromExcel = split[i].substring(6, split[i].length());
				List<WebElement> itemName = driver
						.findElements(By.xpath("(//div[contains(text(),'" + itemfromExcel + "')])"));
				for (WebElement wb : itemName) {
					wb.isDisplayed();
					String actaulItemFromWebPage = wb.getText();
					Assert.assertEquals(itemfromExcel, actaulItemFromWebPage);
					wrapFunc.highLightElement(wb);
					System.out.println("Actual from Mulitpe:" + actaulItemFromWebPage);
					System.out.println("Item from Excel" + itemfromExcel);
				}
			}

		}
		String itemfromExcel = fetchValuesFromSpreadSheet.substring(6, fetchValuesFromSpreadSheet.length());
		WebElement wb = driver.findElement(By.xpath("(//div[contains(text(),'" + itemfromExcel + "')])[2]"));
		wb.isDisplayed();
		String actaulItemFromWebPage = wb.getText();
		Assert.assertEquals(itemfromExcel, actaulItemFromWebPage);
		wrapFunc.highLightElement(wb);
		System.out.println("Actual from Mulitpe:" + actaulItemFromWebPage);
		System.out.println("Item from Excel" + itemfromExcel);

	}

	public static void updateValuesToExcel(String sheetname, String columnname, String value, int rowNum)
			throws Exception {
		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row row = sheet.getRow(0);
			for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {

				Cell cell = row.getCell(i);
				String stringCellValue = cell.getStringCellValue();
				if (stringCellValue.equals(columnname)) {
					Row data = sheet.getRow(rowNum);
					if (data == null) {
						data = sheet.createRow(rowNum);
					}

					Cell c1 = data.getCell(i);
					if (c1 == null) {
						c1 = data.createCell(i);
					}
					c1.setCellValue(value);
				}
			}
			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		} //

	}

	public static void updateDateToExcel(String sheetname, String columnname, Date value, int rowNum) throws Exception {
		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row row = sheet.getRow(0);
			for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {

				Cell cell = row.getCell(i);
				String stringCellValue = cell.getStringCellValue();
				if (stringCellValue.equals(columnname)) {
					Row data = sheet.getRow(rowNum);
					if (data == null) {
						data = sheet.createRow(rowNum);
					}

					Cell c1 = data.getCell(i);
					if (c1 == null) {
						c1 = data.createCell(i);
					}
					c1.setCellValue(value);
				}
			}
			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		} //

	}

	@Given("^I fetch values '(.*)' from webpage and store as '(.*)'$")
	public static void i_fetch_values_from_the_webpage() {

	}

	@Given("^I enter '(.*)' : '(.*)' from spreadsheet in the feild '(.*)'$")
	public static void i_enter_values_in_the_spreasheet(String sheet, String column, String feild) throws Exception {
		try {
			String fetchValuesFromSpreadSheet = fetchValuesFromSpreadSheet(sheet, column);
			CommonSteps.I_enter_in_field(fetchValuesFromSpreadSheet, feild);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I validate '(.*)' from '(.*)' sheet contained at '(.*)'$")
	public static void ReadExcelValueIsContained(String Value, String sheetname, String element) throws Exception {

		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue = "";
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();

			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				if (hr.getCell(i).getStringCellValue().equals(Value)) {
					cellValue = content.getCell(i).getStringCellValue().toString();
					System.out.println(containerText);
					Assert.assertTrue(containerText.contains(cellValue));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	
	@When("^I click shipping dropdown button if displayed$")
    public static void i_click_Shipping_Dropdown() {
        try {
            Select Shipping_Dropdown = new Select(driver.findElement(By.id("deliveryMethodQuestion")));
            Shipping_Dropdown.selectByValue("TYPE_3");
        } catch (Exception e) {
            System.out.println("Shipping dropdown is not displayed ...");
        }
    }
	
	
	@Given("^I find Total at '(.*)' from '(.*)' sheet contained at '(.*)'$")
	public static void ReadExcelValueIsContainedTotal(String Amount, String sheetname, String element)
			throws Exception {

		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue = "";
			String TotalValue = "";
			String Total = "";
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();

			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {

				if (hr.getCell(i).getStringCellValue().equals(Amount)) {
					cellValue = content.getCell(i).getStringCellValue().toString();
					TotalValue = StringUtils.stripStart(cellValue, "$");
					Total = "USD " + TotalValue;
				}
			}
			Assert.assertEquals(containerText, Total);
//			Assert.assertTrue(containerText.contains(Total));
			System.out.println(Total);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I find PONumber at '(.*)' from '(.*)' sheet contained at '(.*)'$")
	public static void ReadExcelValueIsContainedPONumber(String PONumber, String sheetname, String element)
			throws Exception {

		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue = "";
			String TotalValue = "";
			String Total = "";
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();

			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {

				if (hr.getCell(i).getStringCellValue().equals(PONumber)) {
					cellValue = content.getCell(i).getStringCellValue().toString();
					TotalValue = StringUtils.stripStart(cellValue, "PO Number:");
					Total = TotalValue;
				}
			}
			Assert.assertTrue(containerText.contains(Total));
			System.out.println(Total);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I find Quantity at '(.*)' from '(.*)' sheet contained at '(.*)'$")
	public static void ReadExcelValueIsContainedQuantity(String value, String sheetname, String element)
			throws Exception {

		try {
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();
			String Quantity = fetchValuesFromSpreadSheet + ".00";
			Assert.assertEquals(containerText, Quantity);
//			Assert.assertTrue(containerText.contains(Quantity));
			System.out.println(Quantity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I find Product at '(.*)' from '(.*)' sheet contained at '(.*)'$")
	public static void ReadExcelValueIsContainedProduct(String value, String sheetname, String element)
			throws Exception {
		String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
		String TotalValue = StringUtils.stripStart(fetchValuesFromSpreadSheet, "Item #:");
		WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
		String containerText = container.getText().toString();
			Assert.assertEquals(containerText, TotalValue);
			System.out.println(value);
		}

	@Given("^I concat '(.*)' and '(.*)' from '(.*)' sheet is contained at '(.*)'$")
	public static void registerSalesforce(String Value1, String Value2, String sheetname, String element)
			throws Exception {

		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue1 = "";
			String cellValue2 = "";
			String cellValue3 = "";
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();

			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				if (hr.getCell(i).getStringCellValue().equals(Value1)) {
					cellValue1 = content.getCell(i).getStringCellValue();
				}

				if (hr.getCell(i).getStringCellValue().equals(Value2)) {
					cellValue2 = content.getCell(i).getStringCellValue();
				}

				cellValue3 = cellValue1 + " " + cellValue2;
			}
			System.out.println(cellValue3);
			Assert.assertTrue(containerText.contains(cellValue3));

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I enter '(.*)' from '(.*)' sheet to the field '(.*)'$")
	public static void enter_field(String Value, String sheetname, String element) throws Exception {

		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue = "";
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				if (hr.getCell(i).getStringCellValue().equals(Value)) {
					cellValue = content.getCell(i).getStringCellValue();
				}
			}
			container.sendKeys(cellValue);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I get the details '(.*)' from '(.*)' and store as '(.*)'$")
	public static void i_get_the_details_from_orderdetails_page(String feild, String sheet, String values)
			throws Exception {

		List<WebElement> elements = driver.findElements(GetPageObjectRead.OR_GetElement(feild));
		// List<String> elements = new ArrayList<String>();

		// elements.add("Hello");
		// elements.add("Yes");
		if (elements.size() > 1) {
			for (int i = 1; i <= elements.size(); i++) {
				String text = elements.get(i - 1).getText();
//				System.out.println(text);

				updateValuesToExcel(sheet, values, text, i);
				// System.out.println(i);
				// System.out.println(elements.get(i-1));
			}
		} else {
			WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(feild));
			String text = element.getText();
			updateValuesToExcel(sheet, values, text, 1);
//			System.out.println(text);
		}

	}

	@Given("^I remove items from cart$")
	public static void i_clear_the_cart_for_omega_application() throws Exception {
		CommonSteps.I_Refresh_WebPage();
		CommonSteps.I_click("Minicart");
		try {
			driver.findElement(By.xpath("//div[@class='nav-cart active']//..//a[contains(text(),'View/Edit Cart')]")).click();
	//		CommonSteps.I_click("Checkout_MiniCart");
			CommonSteps.I_should_see_on_page("Checkout");
			List<WebElement> removeBtn = driver.findElements(By.xpath("(//span[@data-entry-action='REMOVE'])"));
			for (int i = 1; i <= removeBtn.size(); i++) {
				if (i % 2 == 1) {
					driver.findElement(By.xpath("(//span[@data-entry-action='REMOVE'])[1]")).click();;					WebElement productRemoveAlert = driver.findElement(By.xpath("//div[@class='global-alerts']//div"));
					productRemoveAlert.isDisplayed();
					wrapFunc.highLightElement(productRemoveAlert);
				}
			}
			WebElement emptyCart = driver.findElement(By.xpath("//h2[text()='Your shopping cart is empty']"));
			emptyCart.isDisplayed();
			WrapperFunctions.highLightElement(emptyCart);
			;
			CommonSteps.I_click("OmegaLogo");

		} catch (org.openqa.selenium.NoSuchElementException e) {
			driver.findElement(By.xpath("(//div[text()='Empty Cart'])[1]")).isDisplayed();
			wrapFunc.highLightElement(driver.findElement(By.xpath("(//div[text()='Empty Cart'])[1]")));
		}

	}

	@Given("^I get the date '(.*)' from '(.*)' and store as '(.*)'$")
	public static void i_get_the_details_from_page(String feild, String sheet, String values) throws Exception {

		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(feild));
		String text = element.getAttribute("value");
		System.out.println("Date: " + text);
		Date date = new SimpleDateFormat("MM/dd/yyyy").parse(text);
		System.out.println("Date: " + date);
		updateDateToExcel(sheet, values, date, 1);

	}

	@Given("^I enter date '(.*)' from '(.*)' sheet at the '(.*)'$")
	public static void i_enter_date_from_excelSheet_NP(String value, String sheetname, String location)
			throws Exception {
		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			Date cellValue;
			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				if (hr.getCell(i).getStringCellValue().equals(value)) {
					cellValue = content.getCell(i).getDateCellValue();
					SimpleDateFormat Dvalue = new SimpleDateFormat("MM/dd/yyyy");
					String DateValue = Dvalue.format(cellValue);
					driver.findElement(GetPageObjectRead.OR_GetElement(location)).sendKeys(DateValue);
					System.out.println("Date: " + DateValue.toString());
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		// i_get_the_details_from_orderdetails_page("", "PinCode");
//		randomDiscount();
//		i_get_60_days_before_given_date();
	
	}

	@When("^I change the screen dimension to '(.*)' - '(.*)'$")
	public static void i_change_the_screen_dimension(int width, int height) throws Exception {
		try {
			driver.manage().window().setSize(new Dimension(width, height));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}
	}

	@When("^I navigate to '(.*)' application$")
	public static void i_navigate_to_application(String url) {
		try {
			String UpdateUrl = url;
			driver.navigate().to(UpdateUrl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);

		}
	}

	@When("^I click the RMA Number '(.*)'$")
	public static void i_click_rma_number(String number) throws Exception {
		try {
			String no = HashMapContainer.get(number);
			driver.findElement(By.xpath("//a[contains(text(),'" + no + "')]")).click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}
	}

	@When("^I get the RMA number from confirmation message and store as '(.*)'")
	public static void i_get_rma_number_from_confirmation_message(String element) throws Exception {
		try {
			String text = driver.findElement(GetPageObjectRead.OR_GetElement("Success_Message")).getText();
			String[] split = text.split(" ");
			String rma = split[8].substring(0, split[8].length() - 1);
			HashMapContainer.add("$$" + element, rma);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}

	}

	@When("^I add scenario name$")
	public static void i_add_scenario_name() throws InterruptedException {
		Thread.sleep(5000);
		Cookie cookie = new Cookie("zaleniumMessage", "Go to home page");
		driver.manage().addCookie(cookie);
		Thread.sleep(5000);

	}

	@When("^I press key: enter$")
	public static void i_press_enter_key_() throws Exception {
		try {
			Actions ac = new Actions(driver);
			ac.sendKeys(Keys.ENTER).build().perform();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}
	}

//	@Given("^I get all element locators '(.*)' : '(.*)' on to file name: '(.*)'$")
//	public static void i_capture_locators_in_the_page(String key, String value, String fileName) throws Throwable {
//		try {
//			System.out.println(driver.getCurrentUrl());
//			LocatorGenerator.createExcel(CommonSteps.appName, fileName, driver.getCurrentUrl());
//			LocatorGenerator.getTagChildElementsbyId(driver.getPageSource(), key, value, true);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			throw new CucumberException(e.getMessage(), e);
//		}
//	}

	@Given("^I verify title of the page in US$")
	public static void I_verify_title_of_the_page_in_US() throws Exception {
		try {
			String actualTitle = driver.getTitle();
			System.out.println(actualTitle);
			String expectedTitle = "About Us | Omega Engineering US Site";
			Assert.assertEquals(expectedTitle, actualTitle);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Then("^I wait for element present '(.*)'")
	public static void I_wait_for_element_present(String element) throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(GetPageObjectRead.OR_GetElement(element)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I verify title of the page in French$")
	public static void I_verify_title_of_the_page_FR() throws Exception {
		try {
			String actualTitle = driver.getTitle();
			System.out.println(actualTitle);
			String expectedTitle = "� propos de nous | Omega Engineering";
			Assert.assertEquals(expectedTitle, actualTitle);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Then("^I remove Captcha element")
	public static void I_remove_Captcha() {
		try {
			WebElement yourElement = driver.findElement(By.xpath("//div[@class='g-recaptcha']"));
			JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
			jsExecutor.executeScript("arguments[0].parentNode.removeChild(arguments[0])", yourElement);
			Thread.sleep(3000);
			JavascriptExecutor js = (JavascriptExecutor) driver;
//			js.executeScript("return document.getElementByXpath('//input[@value='Submit']').disabled=false;");

			js.executeScript("document.getElementById('bt_submit').removeAttribute('disabled');");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	/*
	 * @Then("^I should see text '(.*)' present on page$") public static void
	 * I_should_see_text_present_on_page(String expectedText) throws Exception {
	 * 
	 * try { CommonSteps.I_should_see_text_present_on_page(expectedText); } catch
	 * (Exception e) { // TODO Auto-generated catch block e.printStackTrace(); throw
	 * new Exception();
	 * 
	 * } }
	 */

	@Then("I press enter key")
	public void i_press_enter_key() {
		driver.findElement(By.id("1")).sendKeys(Keys.RETURN);
	}

	@Then("I select US from dropdown")
	public static void i_select_US_from_dropdown() {
		Select country = new Select(driver.findElement(By.className("delivery-country")));
		country.selectByVisibleText("United States");
	}

	@When("I select Quality from dropdown")
	public void i_select_Quality_from_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select type = new Select(driver.findElement(By.id("type")));
		type.selectByValue("Quality");
	}

	@When("I select UPS Ground from dropdown")
	public void i_select_UPS_Ground_from_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select delivery_method = new Select(driver.findElement(By.id("delivery_method")));
		delivery_method.selectByValue("UPS_US");
	}

	@When("I select CA Ground from dropdown")
	public void i_select_CA_from_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select state = new Select(driver.findElement(By.id("00N2F000000iHDo")));
		state.selectByValue("CA");
	}

	@Then("^I enter name in field '(.*)'")
	public static void I_enter_name_in_field(String element) {
		try {
			// Generate a random name
			final String randomName = randomName();

			// Find the email form field
			WrapperFunctions.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));
			WebElement email = driver.findElement(GetPageObjectRead.OR_GetElement(element));

			// Type the random email to the form
			email.sendKeys(randomName);
			System.out.println(randomName);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	private static String randomName() {
		return "random-" + UUID.randomUUID().toString();
	}
	
	@Given("$I should enter random discount at '(.*)'^")
	public static void randomDiscount(String locator) {
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		String RandomDiscount = "Discount-" + UUID.randomUUID().toString()+" 10%";
		System.out.println(RandomDiscount); 
		element.sendKeys(RandomDiscount);
	}

	@When("^I navigate to asm application$")
	public static void i_navigate_to_asm_application() {
		try {
			String currentUrl = driver.getCurrentUrl();
			String url = currentUrl + "?asm=true";
			driver.navigate().to(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);

		}
	}

	@Then("^I register '(.*)'")
	public static void I_register_email(String element) {
		try {
			// Generate a random email

			String uuid = UUID.randomUUID().toString().substring(0, 10);

			final String randomEmail = uuid + "@royalcyber.com";

			// Find the email form field
			WrapperFunctions.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));
			WebElement email = driver.findElement(GetPageObjectRead.OR_GetElement(element));

			// Type the random email to the form
			email.sendKeys(randomEmail);
			HashMapContainer.add("EmailId", randomEmail);
			updateValuesToExcel("Register", "EmailId", randomEmail, 1);
			System.out.println(randomEmail);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	private static String randomEmail() {
		return "random-" + UUID.randomUUID().toString() + "@example.com";
	}

	@When("^I enter username and password to login the application$")
	public static void i_enter_username_password_to_login_the_app() throws Exception {
		try {
			CommonSteps.I_enter_in_field(System.getProperty("UserName", "taha.patel66@royalcyber.com"), "UserName");
			CommonSteps.I_pause_for_seconds(5);
			CommonSteps.I_enter_in_field(System.getProperty("PassWord", "Omega2003@"), "Password");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}

	}

	@Given("^I verify url of the page$")
	public static void I_verify_url_of_the_page() throws Exception {
		try {
			String url = driver.getCurrentUrl();
			System.out.println(url);
			if (url.contains("fr")) {
				System.out.println("Page displayed in French language");
			} else {
				System.out.println("Page NOT displayed in French");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I navigate back$")
	public static void I_navigate_back() throws Exception {
		try {
			driver.navigate().back();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I verify pdf of the page$")
	public static void I_verify_pdf_of_the_page() throws Exception {
		try {
			String url = driver.getCurrentUrl();
			System.out.println(url);
			if (url.contains("pdf")) {
				System.out.println("PDF is displayed");
			} else {
				System.out.println("PDF is not displayed");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I switch to iFrame Configure Product$")
	public static void I_switch_to_iFrame_ConfigureProduct() throws Exception {
		try {
			driver.switchTo().frame("configurationIframe");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I switch to back to default content$")
	public static void I_switch_back_to_default_content() throws Exception {
		try {
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			Thread.sleep(5000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	/*
	 * @Then("^I should see text '(.*)' present on page$") public static void
	 * I_should_see_text_present_on_page(String expectedText) throws Exception {
	 * 
	 * try { CommonSteps.I_should_see_text_present_on_page(expectedText); } catch
	 * (Exception e) { // TODO Auto-generated catch block e.printStackTrace(); throw
	 * new Exception();
	 * 
	 * } }
	 */

	@Then("^I get the value from Support Ticket page '(.*)' and store as '(.*)'$")
	public static void i_get_value_from_Support_Ticket_Page(String element, String customName) throws Exception {
		try {
			String ticket = driver.findElement(GetPageObjectRead.OR_GetElement(element)).getAttribute("value");
			System.out.println(ticket);
			HashMapContainer.add("$$" + customName, ticket);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("Unable to get text from Support Ticket page");
		}

	}

	@Then("^I get the text from Support Ticket page '(.*)' and store as '(.*)'$")
	public static void i_get_text_from_Support_Ticket_Page(String element, String customName) throws Exception {
		try {
			String ticket = driver.findElement(GetPageObjectRead.OR_GetElement(element)).getText();
			System.out.println(ticket);
			HashMapContainer.add("$$" + customName, ticket);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("Unable to get text from Support Ticket page");
		}

	}

	@When("I compare '(.*)' with text Omega '(.*)'$")
	public static void i_compare_ProductName(String string, String string2) throws Exception {
		try {

			String string3 = HashMapContainer.get(string);

			String string4 = HashMapContainer.get(string2);

			Assert.assertEquals(string3, string4);
			System.out.println("Actual and expected text is same");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("Comparison failed");
		}

	}

	@Given("^I verify Spectris Footer is navigating to respective page$")
	public static void I_verify_Spectris_at_Footer_of_the_page() throws Exception {
		try {
			String title = driver.getTitle();
			System.out.println(title);
			if (title.equals("Spectris")) {
				System.out.println("Spectris is navigating to corresponding page");
			} else {
				System.out.println("Spectris is NOT navigating to corresponding page");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@When("^I remove feedback link$")
	public static void i_remove_feedback() {
		WebElement yourElement = driver.findElement(By.xpath("//span[text()='Feedback']"));
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].parentNode.removeChild(arguments[0])", yourElement);
	}

	@When("^I remove need help$")
	public static void i_remove_need_help() {
		WebElement yourElement = driver.findElement(By.xpath("//div[@class='chat_invitation']"));
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].parentNode.removeChild(arguments[0])", yourElement);
	}

	@Given("^I focus and click the button '(.*)'$")
	public static void I_focus_And_click(String element) throws Exception {
		try {
			WrapperFunctions.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));

			Actions ac = new Actions(driver);
			WebElement we = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			ac.moveToElement(we).build().perform();
			ac.click().build().perform();

		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("I select United States from dropdown")
	public void i_select_United_States_from_dropdown() {
		Select country = new Select(driver.findElement(By.id("address.country")));
		country.selectByVisibleText("United States");
	}

	@When("I select Albama from dropdown")
	public void i_select_Albama_from_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select state = new Select(driver.findElement(By.id("00N2F000000iHDo")));
		state.selectByValue("AL");
	}

	@When("I select 3-10 in dropdown")
	public void i_select_3_10_in_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select state = new Select(driver.findElement(By.id("00N2F000000hLJF")));
		state.selectByValue("3-10");
	}

	@When("I select option Government Agency from dropdown")
	public void i_select_option_Government_Agency_from_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select state = new Select(driver.findElement(By.id("00N2F000000hQOy")));
		state.selectByValue("Government Agency");
	}
}