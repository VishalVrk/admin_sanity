package com.TEAF.stepDefinitions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Driver;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.winium.WiniumDriver;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;

public class SytelineCustomSteps {

	public static WiniumDriver winDriver=CommonStepsWindows.winDriver;
	
	@Given("^I enter Order Number '(.*)' from '(.*)' sheet in the Order Pane$")
	public static void i_enter_order_Number_inthe_orderpane(String value,String sheetname) throws Exception {
		try {
			java.io.File f = new java.io.File(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue ="";
			
			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				if(hr.getCell(i).getStringCellValue().equals(value)) {
				cellValue = content.getCell(i).getStringCellValue().toString();
				winDriver.findElement(By.id("CoNumEdit")).findElement(By.id("maskedEdit")).sendKeys(cellValue, Keys.ENTER);
				}
				}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
			throw new Exception();
		}
	}
	
	@Given("^I enter '(.*)' from the '(.*)' Pane$")
	public static void i_enter_order_Number_inthe_DueDate(String value, String location) throws Exception {
		try {
				winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit")).sendKeys(value);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
			throw new Exception();
		}
	}
	
	

	
	@Given("^I enter '(.*)' from '(.*)' sheet at the '(.*)' Pane$")
	public static void i_enter_from_excelSheet(String value,String sheetname,String location) throws Exception {
		try {
			java.io.File f = new java.io.File(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue ="";	
			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				if(hr.getCell(i).getStringCellValue().equals(value)) {
				cellValue = content.getCell(i).getStringCellValue().toString();
				System.out.println(cellValue.toString());
				}
				}
			
			winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit")).sendKeys(cellValue.toString());
			System.out.println(cellValue.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
		}
	}
	
	@Given("^I enter date '(.*)' from '(.*)' sheet at the '(.*)' Pane$")
	public static void i_enter_date_from_excelSheet(String value,String sheetname,String location) throws Exception {
		try {
			java.io.File f = new java.io.File(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			Date cellValue;	
			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				if(hr.getCell(i).getStringCellValue().equals(value)) {
				cellValue = content.getCell(i).getDateCellValue();
				SimpleDateFormat Dvalue = new SimpleDateFormat("dd-MM-yyyy");
				String DateValue = Dvalue.format(cellValue);
				winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit")).sendKeys(DateValue.toString());
				System.out.println(DateValue.toString());
				}
				}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
		}
	}
	
	
	@Given("^I store '(.*)' from '(.*)' sheet at '(.*)' Pane$")
	public static void i_get_name_from_field(String value,String sheetname,String location) throws Exception {
		try {
			java.io.File f = new java.io.File(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String AttributeValue=null;
			for(int i=0; i < hr.getPhysicalNumberOfCells();i++) {
			if(hr.getCell(i).getStringCellValue().equals(value)) {
				AttributeValue =winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("Name");
				Cell cell = content.getCell(i);
				cell.setCellValue(AttributeValue);
				System.out.println(AttributeValue+": "+i+" "+value+" "+sheetname+" "+location);
			}
			}
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
			throw new Exception();
		}
	}
	@Given("^I get the date '(.*)' from '(.*)' and store as '(.*)' Pane$")
	public static void i_get_the_details_from_page_pane(String feild,String sheet, String values) throws Exception {
		
		String AttributeValue =winDriver.findElement(GetPageObjectRead.OR_GetElement(feild)).getAttribute("Name");
		System.out.println("Date: "+AttributeValue);
		Date date = new SimpleDateFormat("MM-dd-yyyy").parse(AttributeValue);
		System.out.println("Date: "+date);
		Omega_CustomMethods.updateDateToExcel(sheet, values, date , 1);
		
	}
	
	
	@Given("^I clear '(.*)' field Pane$")
	public static void i_clear_field(String location) throws Exception {
		try {
				winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit")).clear();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
			throw new Exception();
		}
	}
	
	@Given("^I enter '(.*)' from the '(.*)' Pane using actions$")
	public static void i_enter_order_Number_inthe_send(String value, String location) throws Exception {
		try {
			Actions act = new Actions(winDriver);
//			act.click(winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit"))).build().perform();
			Thread.sleep(5000);
			act.sendKeys(winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit")), value).build().perform();
			winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit")).sendKeys(value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
		}
	}
	
	@Given("^I enter '(.*)' from '(.*)' sheet at the '(.*)' Pane using actions$")
	public static void i_enter_value_from_excelSheet_using_actions(String value,String sheetname, String location) throws Exception {
		try {
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			Actions act = new Actions(winDriver);
//			act.click(winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit"))).build().perform();
			Thread.sleep(5000);
			act.sendKeys(winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit")), fetchValuesFromSpreadSheet).build().perform();
			winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit")).sendKeys(fetchValuesFromSpreadSheet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
		}
	}
	
	@Then("^I enter '(.*)' in field from '(.*)' sheet at '(.*)' of window$")
	public static void I_enter_in_field_ofWindow_from_excelSheet(String value,String sheetname ,String element) {
		try {
			//wrapFunc.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));
			//winDriver.findElement(GetPageObject.OR_GetElement(element)).clear();
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			if(value.length()>3){
				if(value.substring(0,2).equals("$$"))
				{
					System.out.println("Fetching from HMcontainer!");
					value = HashMapContainer.get(value);
				}else if(value.substring(0, 3).equals("$XS")) {
					String randomVal =  value.replaceAll("\\"+value.substring(0,4), Utilities.getRandomString(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$"+value, randomVal );
					value = randomVal;
				}else if(value.substring(0, 3).equals("$XE")) {
					String randomVal = Utilities.GetRandomEmail(Character.getNumericValue(value.charAt(3)));
					HashMapContainer.add("$$"+value, randomVal );
					value = randomVal;
				}else if(value.substring(0, 3).equals("$XN")) {
					String randomVal = value.replaceAll("\\"+value.substring(0,4), Utilities.getRandomNum(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$"+value, randomVal );
					value = randomVal;
				}else if(value.substring(0, 3).equals("$XA")) {
					String randomVal = value.replaceAll("\\"+value.substring(0,4), Utilities.getRandomAlphaNumeric(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$"+value, randomVal );
					value = randomVal;
				}
			}
			winDriver.findElement(GetPageObjectRead.OR_GetElement(element)).sendKeys(fetchValuesFromSpreadSheet);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}
	
	@Given("^I click '(.*)' Pane$")
	public static void i_click_panes(String location) throws Exception {
		try {
				winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).findElement(By.id("maskedEdit")).click();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
			throw new Exception();
		}
	}
	
	@Then("^I should see '(.*)' from '(.*)' sheet present on window at '(.*)'$")
	public static void I_should_see_text_contained_present_on_window_At(String value,String sheetname, String location) {
		try {
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("Name");
				Assert.assertEquals(fetchValuesFromSpreadSheet, actualText);
				StepBase.embedScreenshot(); 
			} catch (Exception e) {
				e.printStackTrace();
				throw new CucumberException(e.getMessage(), e);
			}
	}
	
	
	@Then("^I find item '(.*)' from '(.*)' sheet present on window at '(.*)'$")
	public static void I_should_see_itemName_contained_present_on_window_At(String value,String sheetname, String location) {
		try {
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			String itemName = StringUtils.stripStart(fetchValuesFromSpreadSheet, "Item #:");
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("Name");
				Assert.assertEquals(itemName, actualText);
				StepBase.embedScreenshot(); 
			} catch (Exception e) {
				e.printStackTrace();
				throw new CucumberException(e.getMessage(), e);
			}
	}
	
	@Then("^I find UnitPrice '(.*)' from '(.*)' sheet present on window at '(.*)'$")
	public static void I_should_see_unitPrice_contained_present_on_window_At(String value,String sheetname, String location) {
		try {
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			String UnitPrice =StringUtils.stripStart(fetchValuesFromSpreadSheet, "$")+"000";
			String Value = "      "+UnitPrice;
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("Name");
				Assert.assertEquals(Value, actualText);
				StepBase.embedScreenshot(); 
			} catch (Exception e) {
				e.printStackTrace();
				throw new CucumberException(e.getMessage(), e);
			}
	}
	
	@Then("^I find NetPrice '(.*)' from '(.*)' sheet present on window at '(.*)'$")
	public static void I_should_see_NetPrice_contained_present_on_window_At(String value,String sheetname, String location) {
		try {
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			String NetPrice =StringUtils.stripStart(fetchValuesFromSpreadSheet, "$");
			String Value = "      "+NetPrice;
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("Name");
				Assert.assertEquals(Value, actualText);
				StepBase.embedScreenshot(); 
			} catch (Exception e) {
				e.printStackTrace();
				throw new CucumberException(e.getMessage(), e);
			}
	}
	
	@Then("^I find Quantity '(.*)' from '(.*)' sheet present on window at '(.*)'$")
	public static void I_should_see_Quantity_contained_present_on_window_At(String value,String sheetname, String location) {
		try {
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			String Quantity =fetchValuesFromSpreadSheet+".000";
			String Value = "      "+Quantity;
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("Name");
				Assert.assertEquals(Value, actualText);
				StepBase.embedScreenshot(); 
			} catch (Exception e) {
				e.printStackTrace();
				throw new CucumberException(e.getMessage(), e);
			}
	}
	
	@Then("^I find ShipTo '(.*)' from '(.*)' sheet present on window at '(.*)'$")
	public static void I_should_see_ShipTo_contained_present_on_window_At(String value,String sheetname, String location) {
		try {
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			String CustomerNumber = Omega_CustomMethods.fetchValuesFromSpreadSheet("AccountPayment","CustomerNumber");
			String removeVal =CustomerNumber+"-";
			String ShipTo =StringUtils.stripStart(fetchValuesFromSpreadSheet,removeVal);
			String Value = "   "+ShipTo;
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("Name");
				Assert.assertEquals(Value, actualText);
				StepBase.embedScreenshot(); 
			} catch (Exception e) {
				e.printStackTrace();
				throw new CucumberException(e.getMessage(), e);
			}
	}
	
	@Then("^I find PONumber see '(.*)' from '(.*)' sheet present on window at '(.*)'$")
	public static void I_should_see_ponumber_contained_present_on_window_At(String value,String sheetname, String location) {
		try {
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			String PONumber = StringUtils.stripStart(fetchValuesFromSpreadSheet, "PO Number:");
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("Name");
				Assert.assertEquals(PONumber, actualText);
				StepBase.embedScreenshot(); 
			} catch (Exception e) {
				e.printStackTrace();
				throw new CucumberException(e.getMessage(), e);
			}
	}
	
	@Then("^I find UPS see '(.*)' from '(.*)' sheet present on window at '(.*)'$")
	public static void I_should_see_order_contained_present_on_window_At(String value,String sheetname, String location) {
		try {
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, value);
			String UPS = StringUtils.stripEnd(fetchValuesFromSpreadSheet, "Ground - $15.00");
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("Name");
				Assert.assertEquals(UPS, actualText);
				StepBase.embedScreenshot(); 
			} catch (Exception e) {
				e.printStackTrace();
				throw new CucumberException(e.getMessage(), e);
			}
	}
	
	@When("^I wait for 15 mins to reflect changes in integration system$")
	public static void i_wait_for_15_min_reflect() throws InterruptedException, Exception {
		File des = new File(System.getProperty("user.dir")+"\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
		String pathname = System.getProperty("ExcelPath", "c:\"");
		File src = new File(pathname+"\\OmegaTestDatas.xlsx");
		FileUtils.copyFile(src, des);
		System.out.println("Current Time: "+LocalTime.now().getHour()+":"+LocalTime.now().getMinute());
		Date d = new Date(System.currentTimeMillis() + 15*60*1000);
		System.out.println("Execution will start at");
		System.out.println("Hours " + d.getHours() + " and Minutes " + d.getMinutes());
//		Thread.sleep(15*60*1000);
	}
	
	@When("^I wait for 25 mins to reflect changes in integration system$")
	public static void i_wait_for_25_min_reflect() throws  Exception {
		File des = new File(System.getProperty("user.dir")+"\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
		String pathname = System.getProperty("ExcelPath", "c:\"");
		File src = new File(pathname+"\\OmegaTestDatas.xlsx");
		FileUtils.copyFile(src, des);
		System.out.println("Current Time: "+LocalTime.now().getHour()+":"+LocalTime.now().getMinute());
		Date d = new Date(System.currentTimeMillis() + 25*60*1000);
		System.out.println("Execution will start at");
		System.out.println("Hours " + d.getHours() + " and Minutes " + d.getMinutes());
//		Thread.sleep(25*60*1000);
	}
	
	@Then("^I should see contained '(.*)' present on window at '(.*)'$")
	public static void I_should_see_text_present_on_window_At(String expectedText, String location) {
		try {
			
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getText();

			System.out.println(actualText);

			//	actualText = actualText.substring(11);
				if (actualText.contains(expectedText)) {
						Assert.assertTrue(true);
					//	mouseHovertoWindowsLocator(location);
					//	highLightElement(winDriver.findElement(GetPageObjectRead.OR_GetElement(location)));

				}else {
					Assert.fail("Expected contained text is "+ expectedText +" but Actual is "+ actualText);
				}
				StepBase.embedScreenshot(); 
			} catch (Exception e) {
				e.printStackTrace();
				throw new CucumberException(e.getMessage(), e);
			}
	}
}
