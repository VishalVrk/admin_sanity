package com.TEAF.stepDefinitions;

import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;

import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class CustomStepsAdmin_Storefront {
	static WrapperFunctions wrapFunc = new WrapperFunctions();
	static StepBase sb = new StepBase();
	// static StepBase_D2 sb = new StepBase_D2();
	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();
	
	
	@Given("^I fetch values and store in excel sheet '(.*)' : Sheet$")
	public static void i_fetch_values_and_store_in_excel_sheet(String sheetnames) throws Exception {
		CommonSteps.I_should_see_on_page("OrderNumber");
		CommonSteps.I_should_see_on_page("OrderPlacedBy");
		CommonSteps.I_should_see_on_page("OrderStatus");
		CommonSteps.I_should_see_on_page("DatePlaced");
		CommonSteps.I_should_see_on_page("Total");
		CommonSteps.I_should_see_on_page("ShipTo");
		CommonSteps.I_should_see_on_page("ShippingMethod");
		CommonSteps.I_scroll_to_element("coordinates", "0,250");
		CommonSteps.I_should_see_on_page("ProductName");
		CommonSteps.I_should_see_on_page("ItemName");
		CommonSteps.I_should_see_on_page("UnitPrice1");
		CommonSteps.I_should_see_on_page("Quantity");
		CommonSteps.I_should_see_on_page("TotalPrice");
		CommonSteps.I_should_see_on_page("BillingAddress");
		CommonSteps.I_should_see_on_page("OrderTotal");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderNumber",sheetnames, "OrderNumber");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderPlacedBy",sheetnames, "OrderPlacedBy");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderStatus",sheetnames, "OrderStatus");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("DatePlaced",sheetnames, "DatePlaced");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("Total",sheetnames, "Total");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ShipTo",sheetnames, "ShipTo");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ShippingMethod",sheetnames, "ShippingMethod");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ProductName",sheetnames, "ProductName");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ItemName",sheetnames, "ItemName");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("UnitPrice1",sheetnames, "UnitPrice");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("Quantity",sheetnames, "Quantity");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("TotalPrice",sheetnames, "TotalPrice");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("BillingAddress",sheetnames, "BillingAddress");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderTotal",sheetnames, "OrderTotal");
	}
	
	@Given("^I fetch values and store in excel sheet '(.*)' : Sheet multi-line$")
	public static void i_fetch_values_and_store_in_excel_sheet_multi_line(String sheetnames) throws Exception {
		CommonSteps.I_should_see_on_page("OrderNumber");
		CommonSteps.I_should_see_on_page("OrderPlacedBy");
		CommonSteps.I_should_see_on_page("OrderStatus");
		CommonSteps.I_should_see_on_page("DatePlaced");
		CommonSteps.I_should_see_on_page("Total");
		CommonSteps.I_should_see_on_page("ShipTo");
		CommonSteps.I_should_see_on_page("ShippingMethod");
		CommonSteps.I_scroll_to_element("coordinates", "0,250");
		CommonSteps.I_should_see_on_page("ProductName1");
		CommonSteps.I_should_see_on_page("ItemName1");
		CommonSteps.I_should_see_on_page("UnitPrice1");
		CommonSteps.I_should_see_on_page("Quantity1");
		CommonSteps.I_should_see_on_page("TotalPrice1");
		CommonSteps.I_should_see_on_page("ItemName2");
		CommonSteps.I_should_see_on_page("UnitPrice2");
		CommonSteps.I_should_see_on_page("Quantity2");
		CommonSteps.I_should_see_on_page("TotalPrice2");
		CommonSteps.I_should_see_on_page("BillingAddress");
		CommonSteps.I_should_see_on_page("OrderTotal");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderNumber",sheetnames, "OrderNumber");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderPlacedBy",sheetnames, "OrderPlacedBy");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderStatus",sheetnames, "OrderStatus");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("DatePlaced",sheetnames, "DatePlaced");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("Total",sheetnames, "Total");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ShipTo",sheetnames, "ShipTo");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ShippingMethod",sheetnames, "ShippingMethod");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ProductName1",sheetnames, "ProductName1");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ItemName1",sheetnames, "ItemName1");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("UnitPrice1",sheetnames, "UnitPrice1");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("Quantity1",sheetnames, "Quantity1");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("TotalPrice1",sheetnames, "TotalPrice1");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ProductName2",sheetnames, "ProductName2");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ItemName2",sheetnames, "ItemName2");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("UnitPrice2",sheetnames, "UnitPrice2");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("Quantity2",sheetnames, "Quantity2");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("TotalPrice2",sheetnames, "TotalPrice2");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("BillingAddress",sheetnames, "BillingAddress");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderTotal",sheetnames, "OrderTotal");
	}
	
	@Given("^I fetch Quote values and store in excel sheet '(.*)' : Sheet$")
	public static void i_fetch_details_for_Quote_and_store(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("QuoteNumber");
		CommonSteps.I_should_see_on_page("QuoteVersion");
		CommonSteps.I_should_see_on_page("QuoteStatus");
		CommonSteps.I_should_see_on_page("QuoteDateUpdated");
		CommonSteps.I_should_see_on_page("QuoteDateCreated");
		CommonSteps.I_should_see_on_page("QuoteExpiryDate");
		CommonSteps.I_should_see_on_page("QuoteName");
		CommonSteps.I_scroll_to_element("coordinates", "0,250");
		CommonSteps.I_should_see_on_page("QuoteDescription");
		CommonSteps.I_should_see_on_page("QuotePreviousEstimatedTotal");
		CommonSteps.I_should_see_on_page("QuoteProductName1");
		CommonSteps.I_should_see_on_page("QuoteItemName1");
		CommonSteps.I_should_see_on_page("QuoteUnitPrice1");
		CommonSteps.I_should_see_on_page("QuoteQuantity1");
		CommonSteps.I_should_see_on_page("QuoteTotalPrice");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteNumber",sheetname, "QuoteNumber");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteVersion",sheetname, "QuoteVersion");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteStatus",sheetname, "QuoteStatus");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteDateUpdated",sheetname, "DateUpdated");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteDateCreated",sheetname, "DateCreated");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteExpiryDate",sheetname, "ExpiryDate");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteName",sheetname, "Name");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteDescription",sheetname, "Description");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuotePreviousEstimatedTotal",sheetname, "PreviousEstimatedTotal");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteProductName1",sheetname, "ProductName");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteItemName1",sheetname, "ItemName");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteUnitPrice1",sheetname, "Quantity");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteQuantity1",sheetname, "UnitPrice");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("QuoteTotalPrice",sheetname, "TotalPrice");
	}
	
	@When("^I load the excel sheet data to validate the Syteline$")
	public static void i_load_the_excel_sheet_data_to_validate_syteline() throws IOException {
		java.io.File des = new java.io.File(
				System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
		String pathname = System.getProperty("ExcelPath", "c:\"");
		java.io.File src = new java.io.File(pathname + "\\OmegaTestDatas.xlsx");
		FileUtils.copyFile(src, des);
		if (des.isFile()) {
			System.out.println("File Copied");
		}
	}

}
