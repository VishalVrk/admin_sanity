package com.TEAF.stepDefinitions;

import org.openqa.selenium.WebDriver;

import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;

import cucumber.api.java.en.Given;

public class CustomStepsAdmin_Salesforce {
	static WrapperFunctions wrapFunc = new WrapperFunctions();
	static StepBase sb = new StepBase();
	// static StepBase_D2 sb = new StepBase_D2();
	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();
	
	@Given("^I Login Salesforce Application$")
	public static void i_login_in_salesforce_application() {
		Omega_CustomMethods.i_navigate_to_application("https://omega--omegadev.lightning.force.com");
		CommonSteps.I_wait_for_presence_of_element(5, "OmegaSDFC_Logo");
		CommonSteps.I_should_see_on_page("OmegaSDFC_Logo");
		CommonSteps.I_should_see_on_page("SalesforceEmail");
		StepBase.embedScreenshot();
		CommonSteps.I_enter_in_field("tpatel@omega.com", "SalesforceEmail");
		CommonSteps.I_click("SalesforceNext");
		CommonSteps.I_should_see_on_page("SalesforcePassword");
		CommonSteps.I_enter_in_field("Cyber2003!@#$", "SalesforcePassword");
		CommonSteps.I_should_see_on_page("SalesforceVerify");
		CommonSteps.I_click("SalesforceVerify");
		CommonSteps.I_pause_for_seconds(5);
		StepBase.embedScreenshot();
	}
	
	@Given("^I Login Salesforce Application QA$")
	public static void i_login_in_salesforce_application_qa() {
		Omega_CustomMethods.i_navigate_to_application("https://omega--sit.lightning.force.com");
		CommonSteps.I_wait_for_presence_of_element(5, "OmegaSDFC_Logo");
		CommonSteps.I_should_see_on_page("OmegaSDFC_Logo");
		CommonSteps.I_should_see_on_page("SalesforceEmail");
		StepBase.embedScreenshot();
		CommonSteps.i_enter_in_the_feild_using_actions("tpatel@omega.com","SalesforceEmail");
		CommonSteps.I_click("SalesforceNext");
		CommonSteps.I_should_see_on_page("SalesforcePassword");
		CommonSteps.i_enter_in_the_feild_using_actions("Cyber2003!@#$","SalesforcePassword");
		CommonSteps.I_should_see_on_page("SalesforceVerify");
		CommonSteps.I_click("SalesforceVerify");
		CommonSteps.I_pause_for_seconds(5);
	}
	
	@Given("^I validate contents in salesforce from search page from '(.*)' : sheet$")
	public static void i_validate_salesforce_search_page_detail(String sheetname) throws Exception {
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_should_see_on_page("SalesforceOrderName");
		StepBase.embedScreenshot();
		CommonSteps.I_should_see_on_page("SalesforceOrderStatusNumber");
		CommonSteps.I_should_see_on_page("SalesforcePONumber");
		CommonSteps.I_should_see_on_page("SalesforceAccountName");
		CommonSteps.I_should_see_on_page("SalesforceContact");
		CommonSteps.I_should_see_on_page("SalesforceOrderStartDate");
		CommonSteps.I_should_see_on_page("SalesforceStatus");
		CommonSteps.I_should_see_on_page("SalesforceOrderTotal");
		Omega_CustomMethods.ReadExcelValueIsContained("OrderNumber", sheetname, "SalesforceOrderName");
//		Omega_CustomMethods.ReadExcelValueIsContained("PONumber", sheetname, "SalesforcePONumber");
//		Omega_CustomMethods.ReadExcelValueIsContained("AccountName", sheetname, "SalesforceAccountName");
//		Omega_CustomMethods.ReadExcelValueIsContained("OrderPlacedBy", sheetname, "SalesforceContact");
	}
	
	@Given("^I validate header for orders contents in salesforce in '(.*)' : sheet$")
	public static void i_validate_header_elements_in_salesforce_order(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader");
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader_AccName");
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader_Status");
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader_OrderTotal");
		Omega_CustomMethods.ReadExcelValueIsContained("OrderNumber", sheetname, "SalesforceOrderHeader");
	}
	
	@Given("^I validate salesforce details from '(.*)' : sheet$")
	public static void i_validate_order_details_in_salesforce(String sheetname) throws Exception {
		CommonSteps.I_scroll_to_element("coordinates", "0,700");
		CommonSteps.I_should_see_on_page("SalesforceOrderInfo_OrderName");
		CommonSteps.I_should_see_on_page("SalesforceOrderInfo_OrderStartDate");
		CommonSteps.I_should_see_on_page("SalesforceOrderInfo_AccountName");
		CommonSteps.I_should_see_on_page("SalesforceOrderInfo_Status");
		CommonSteps.I_should_see_on_page("SalesforceOrderInfo_OrderSource");
		Omega_CustomMethods.ReadExcelValueIsContained("OrderNumber", sheetname, "SalesforceOrderInfo_OrderName");
		CommonSteps.I_scroll_to_element("coordinates", "0,700");
		CommonSteps.I_should_see_on_page("SalesforceCostsInfo_OrderCustomerNumber");
		CommonSteps.I_should_see_on_page("SalesforceCostsInfo_Contact");
		CommonSteps.I_should_see_on_page("SalesforceCostsInfo_Email");
		CommonSteps.I_should_see_on_page("SalesforceCostsInfo_Subtotal");
		CommonSteps.I_should_see_on_page("SalesforceCostsInfo_ShippingAmount");
		CommonSteps.I_should_see_on_page("SalesforceCostsInfo_DiscountAmount");
		CommonSteps.I_should_see_on_page("SalesforceCostsInfo_SalesTax");
		CommonSteps.I_should_see_on_page("SalesforceCostsInfo_OrderTotal");
		CommonSteps.I_scroll_to_element("coordinates", "0,300");
		CommonSteps.I_should_see_on_page("SalesforceBillingInfo_BillTo");
		CommonSteps.I_should_see_on_page("SalesforceBillingInfo_SytlineBillTo");
		CommonSteps.I_should_see_on_page("SalesforceBillingInfo_PaymentType");
		CommonSteps.I_should_see_on_page("SalesforceBillingInfo_Paymentermss");
		CommonSteps.I_should_see_on_page("SalesforceBillingInfo_PoNumber");
		CommonSteps.I_scroll_to_element("coordinates", "0,300");
		CommonSteps.I_should_see_on_page("SalesforceShippingInfo_ShipToAddress");
		CommonSteps.I_should_see_on_page("SalesforceShippingInfo_SytlineShipTo");
		CommonSteps.I_should_see_on_page("SalesforceShippingInfo_ShippingTermsShipTo");
		CommonSteps.I_should_see_on_page("SalesforceShippingInfo_ShipViaShipTo");
	}
	
	@Given("^I validate order single line items from '(.*)' :sheet$")
	public static void i_validate_line_items_present_for_single_line(String sheetname) throws Exception {
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader");
		CommonSteps.I_scroll_to_element("coordinates", "0,700");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_ProductID");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_Quantity");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_UnitPrice");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_TotalPrice");
		Omega_CustomMethods.ReadExcelValueIsContainedProduct("ItemName", sheetname, "SalesforceOrderLine_ProductID");
		Omega_CustomMethods.ReadExcelValueIsContainedQuantity("Quantity", sheetname, "SalesforceOrderLine_Quantity");
		Omega_CustomMethods.ReadExcelValueIsContainedTotal("UnitPrice", sheetname, "SalesforceOrderLine_UnitPrice");
		Omega_CustomMethods.ReadExcelValueIsContainedTotal("TotalPrice", sheetname, "SalesforceOrderLine_TotalPrice");
	}
	
	@Given("^I validate order single quote line items from '(.*)' :sheet$")
	public static void i_validate_quote_line_items_present_for_single_line(String sheetname) throws Exception {
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader");
		CommonSteps.I_scroll_to_element("coordinates", "0,700");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_ProductID");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_Quantity");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_UnitPrice");
	}
	
	@Given("^I validate order multi line items from '(.*)' :sheet$")
	public static void i_validate_line_items_present_for_multi_line(String sheetname) throws Exception {
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader");
		CommonSteps.I_scroll_to_element("coordinates", "0,700");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_ProductID");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_Quantity");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_UnitPrice");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_TotalPrice");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_ProductID2");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_Quantity2");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_UnitPrice2");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_TotalPrice2");
		Omega_CustomMethods.ReadExcelValueIsContainedProduct("ItemName1", sheetname, "SalesforceOrderLine_ProductID");
		Omega_CustomMethods.ReadExcelValueIsContainedQuantity("Quantity1", sheetname, "SalesforceOrderLine_Quantity");
		Omega_CustomMethods.ReadExcelValueIsContainedTotal("UnitPrice1", sheetname, "SalesforceOrderLine_UnitPrice");
		Omega_CustomMethods.ReadExcelValueIsContainedTotal("TotalPrice1", sheetname, "SalesforceOrderLine_TotalPrice");
		Omega_CustomMethods.ReadExcelValueIsContainedProduct("ItemName2", sheetname, "SalesforceOrderLine_ProductID2");
		Omega_CustomMethods.ReadExcelValueIsContainedQuantity("Quantity2", sheetname, "SalesforceOrderLine_Quantity2");
		Omega_CustomMethods.ReadExcelValueIsContainedTotal("UnitPrice2", sheetname, "SalesforceOrderLine_UnitPrice2");
		Omega_CustomMethods.ReadExcelValueIsContainedTotal("TotalPrice2", sheetname, "SalesforceOrderLine_TotalPrice2");
	}
	
	@Given("^I validate order multi line items from '(.*)' :sheet in reverse$")
	public static void i_validate_multi_line_in_reverse(String sheetname) throws Exception{
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader");
		CommonSteps.I_scroll_to_element("coordinates", "0,700");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_ProductID");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_Quantity");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_UnitPrice");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_TotalPrice");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_ProductID2");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_Quantity2");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_UnitPrice2");
		CommonSteps.I_should_see_on_page("SalesforceOrderLine_TotalPrice2");
		Omega_CustomMethods.ReadExcelValueIsContainedProduct("ItemName2", sheetname, "SalesforceOrderLine_ProductID");
		Omega_CustomMethods.ReadExcelValueIsContainedQuantity("Quantity2", sheetname, "SalesforceOrderLine_Quantity");
		Omega_CustomMethods.ReadExcelValueIsContainedTotal("UnitPrice2", sheetname, "SalesforceOrderLine_UnitPrice");
		Omega_CustomMethods.ReadExcelValueIsContainedTotal("TotalPrice2", sheetname, "SalesforceOrderLine_TotalPrice");
		Omega_CustomMethods.ReadExcelValueIsContainedProduct("ItemName1", sheetname, "SalesforceOrderLine_ProductID2");
		Omega_CustomMethods.ReadExcelValueIsContainedQuantity("Quantity1", sheetname, "SalesforceOrderLine_Quantity2");
		Omega_CustomMethods.ReadExcelValueIsContainedTotal("UnitPrice1", sheetname, "SalesforceOrderLine_UnitPrice2");
		Omega_CustomMethods.ReadExcelValueIsContainedTotal("TotalPrice1", sheetname, "SalesforceOrderLine_TotalPrice2");
		StepBase.embedScreenshot();
	}
	
	@Given("^I validate contents in Quote salesforce from search page from '(.*)' : sheet$")
	public static void i_validate_contents_in_salesforce_from_quote_from_sheet(String sheetname) {
		CommonSteps.I_should_see_on_page("SalesforceQuoteNumber");
		CommonSteps.I_should_see_on_page("SalesforceQuoteContName");
		CommonSteps.I_should_see_on_page("SalesforceQuoteAccountName");
		CommonSteps.I_should_see_on_page("SalesforceQuoteStatus");
		CommonSteps.I_should_see_on_page("SalesforceQuoteStartDate");
		CommonSteps.I_should_see_on_page("SalesforceQuoteExpireDate");
		CommonSteps.I_should_see_on_page("SalesforceQuoteSubtotal");
		CommonSteps.I_should_see_on_page("SalesforceQuoteTotal");
	}
	
	@Given("^I validate Quote salesforce from Header page from '(.*)' : sheet$")
	public static void i_validate_quote_header_contents(String sheetname) {
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader");
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader_QuoteNumer");
		CommonSteps.I_should_see_text_present_on_page_At("Quote for Taha Patel", "SalesforceOrderHeader_OpportunityName");
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader_AccountName");
	}
	
	@Given("^I validate Quote salesforce from Header page from '(.*)' : sheet QA$")
	public static void i_validate_quote_header_contents_QA(String sheetname) {
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader");
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader_QuoteNumer");
		CommonSteps.I_should_see_on_page("SalesforceOrderHeader_OpportunityName");
	}
	
	@Given("^I validate ADMIN details from sheet '(.*)'$")
	public static void i_validate_ADMIN_details_quote(String sheetname) {
		CommonSteps.I_should_see_on_page("SalesforceADMIN_GrandTotal");
		CommonSteps.I_should_see_on_page("SalesforceADMIN_GrandTotal_1");
		CommonSteps.I_should_see_on_page("SalesforceADMIN_HybrisQuoteId");
		CommonSteps.I_should_see_on_page("SalesforceADMIN_LineItems");
		CommonSteps.I_should_see_on_page("SalesforceADMIN_ContactCountry");
		CommonSteps.I_should_see_on_page("SalesforceADMIN_ContactPreferredLanguage");
		CommonSteps.I_should_see_on_page("SalesforceADMIN_PDFQuoteCurrency");
		CommonSteps.I_should_see_on_page("SalesforceADMIN_QuoteDisplayName");
		CommonSteps.I_should_see_on_page("SalesforceADMIN_RecordType");
	}
	
	@Given("^I validate quote information details from sheet '(.*)'$")
	public static void i_validate_quote_information_from_sheet(String sheetname) {
		CommonSteps.I_scroll_to_element("coordinates", "0,200");
		CommonSteps.I_should_see_on_page("SalesforceContactInfo_QuoteNumber");
		CommonSteps.I_should_see_on_page("SalesforceContactInfo_Status");
		CommonSteps.I_should_see_on_page("SalesforceContactInfo_ExpirationDate");
		CommonSteps.I_should_see_on_page("SalesforceContactInfo_Description");
		CommonSteps.I_should_see_on_page("SalesforceContactInfo_MethodOfEntry");
		CommonSteps.I_should_see_on_page("SalesforceContactInfo_OppertunityName");
		CommonSteps.I_should_see_on_page("SalesforceContactInfo_AccountName");
	}
	
	@Given("^I validate quote Totals details from sheet '(.*)'$")
	public static void i_validate_total_details(String sheetname) {
		CommonSteps.I_scroll_to_element("coordinates", "0,200");
		CommonSteps.I_should_see_on_page("SalesforceTotals_SubTotals");
		CommonSteps.I_should_see_on_page("SalesforceTotals_Tax");
		CommonSteps.I_should_see_on_page("SalesforceTotals_TotalPrice");
	}
	
	@Given("^I validate quote Totals details from sheet '(.*)' QA$")
	public static void i_validate_total_details_qa(String sheetname) {
		CommonSteps.I_scroll_to_element("coordinates", "0,200");
		CommonSteps.I_should_see_on_page("SalesforceTotals_SubTotalsQA");
		CommonSteps.I_should_see_on_page("SalesforceTotals_Tax");
		CommonSteps.I_should_see_on_page("SalesforceTotals_TotalPrice");
	}
	
	@Given("^I validate quote Prepared for details from sheet '(.*)'$")
	public static void i_validate_prepared_for(String sheetname) {
		CommonSteps.I_scroll_to_element("coordinates", "0,200");
		CommonSteps.I_should_see_on_page("SalesforceFor_ContactName");
		CommonSteps.I_should_see_on_page("SalesforceFor_Phone");
		CommonSteps.I_should_see_on_page("SalesforceFor_Email");
	}
	

	@Given("^I validate contents from search from '(.*)' : sheet$")
	public static void i_validate_contents_outside_the_page(String sheetname) throws Exception {
		CommonSteps.I_pause_for_seconds(5);
		StepBase.embedScreenshot();
		CommonSteps.I_should_see_on_page("SalesforceOrderName");
		CommonSteps.I_should_see_on_page("SalesforceOrderStatusNumber");
		CommonSteps.I_should_see_on_page("SalesforcePONumber");
		CommonSteps.I_should_see_on_page("SalesforceAccountName");
		CommonSteps.I_should_see_on_page("SalesforceContact");
		CommonSteps.I_should_see_on_page("SalesforceOrderStartDate");
		CommonSteps.I_should_see_on_page("SalesforceStatus");
		CommonSteps.I_should_see_on_page("SalesforceOrderTotal");
		CommonSteps.I_click("SalesforceOrderStatusNumberLink");
	}
}
