package com.TEAF.stepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

//	@Before("")
	public void signinOmegaApplicationforsearch() throws Exception {
		try {
			CommonSteps.my_webapp_is_open("Omega");
			CommonSteps.I_click("SignIn_Btn");
			CommonSteps.I_should_see_on_page("SignID_Field");
			CommonSteps.I_enter_in_field("taha.patel92@royalcyber.com", "SignID_Field");
			CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
			CommonSteps.I_click("LogIn_Btn");
			CommonSteps.I_should_see_text_present_on_page_At("Welcome Taha Patel", "Welcome");
			Omega_CustomMethods.i_clear_the_cart_for_omega_application();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}
	
//	@Before("")
	public void signinOmegaApplicationforsearchQA() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		CommonSteps.I_click("SignIn_Btn");
		CommonSteps.I_should_see_on_page("SignID_Field");
		CommonSteps.I_enter_in_field("taha.patel98@royalcyber.com", "SignID_Field");
		CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
		CommonSteps.I_click("LogIn_Btn");
		CommonSteps.I_should_see_text_present_on_page_At("Welcome Taha Patel", "Welcome");
		
	}
	
	
	// QA Web Customer
//	@Before("")
	public void signinOmegaApplicationforccQA() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		CommonSteps.I_click("SignIn_Btn");
		CommonSteps.I_should_see_on_page("SignID_Field");
		CommonSteps.I_enter_in_field("taha.patel92@royalcyber.com", "SignID_Field");
		CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
		CommonSteps.I_click("LogIn_Btn");
		CommonSteps.I_should_see_text_present_on_page_At("Welcome Taha Patel", "Welcome");
	}
	
	// DEV Web Customer
//	@Before("")
	public void signinOmegaApplicationforcc() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		CommonSteps.I_click("SignIn_Btn");
		CommonSteps.I_should_see_on_page("SignID_Field");
		CommonSteps.I_enter_in_field("taha.patel182@royalcyber.com", "SignID_Field");
		CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
		CommonSteps.I_click("LogIn_Btn");
		CommonSteps.I_should_see_text_present_on_page_At("Welcome Taha Patel", "Welcome");
//		Omega_CustomMethods.i_clear_the_cart_for_omega_application();
	}
	
	
	//Prepay User DEV
	
//	@Before("")
	public void signinOmegaApplicationforpp() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		CommonSteps.I_click("SignIn_Btn");
		CommonSteps.I_should_see_on_page("SignID_Field");
		CommonSteps.I_enter_in_field("taha.patel96@royalcyber.com", "SignID_Field");
		CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
		CommonSteps.I_click("LogIn_Btn");
		CommonSteps.I_should_see_text_present_on_page_At("Welcome Taha Patelll", "Welcome");
//		Omega_CustomMethods.i_clear_the_cart_for_omega_application();
	}
	
	//Prepay User QA
	
//	@Before("")
	public void signinOmegaApplicationforppQA() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		CommonSteps.I_click("SignIn_Btn");
		CommonSteps.I_should_see_on_page("SignID_Field");
		CommonSteps.I_enter_in_field("taha.patel102@royalcyber.com", "SignID_Field");
		CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
		CommonSteps.I_click("LogIn_Btn");
		CommonSteps.I_should_see_text_present_on_page_At("Welcome Taha Patel", "Welcome");
	}
	
	@Before("@OmegaCallLogQA")
	public void signInSytlineQA() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		Omega_CustomMethods.i_navigate_to_application("https://omega.apptrix.com/WSWebClient/Default.aspx");
		CommonSteps.I_clear_Field("UserName");
		CommonSteps.I_clear_Field("PassWord");
		CommonSteps.I_enter_in_field("tpatel", "UserName");
		CommonSteps.I_enter_in_field("pilot123", "PassWord");
		Thread.sleep(3000);
		CommonSteps.I_clear_Field("Config");
		Thread.sleep(3000);
		CommonSteps.I_enter_in_field("pCT01", "Config");
		Thread.sleep(5000);
		Omega_CustomMethods.i_press_enter_key_();
		Thread.sleep(6000);
		CommonSteps.I_click("Login_SL");
		CommonSteps.I_wait_for_visibility_of_element("OpenForm");
		CommonSteps.I_should_see_on_page("OpenForm");
	}
	
	
	@After("@sytline")
	public void signOutOmegaApplication() throws Exception {
		CommonSteps.I_focus_click("systemButton");
		Thread.sleep(5000);
		CommonSteps.I_mouse_over("FormOption_SL");
		CommonSteps.I_focus_click("SignOut_Option");
		Thread.sleep(5000);
		try {
		CommonSteps.I_focus_click("SaveCustomerOrder_NoBtn");
		CommonSteps.I_should_see_on_page("UserName");
		}
		catch (Exception e) {
			CommonSteps.I_should_see_on_page("UserName");
		}
	}
	}

