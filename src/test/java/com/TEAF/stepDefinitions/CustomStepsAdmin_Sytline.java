package com.TEAF.stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;
import junit.framework.Assert;

public class CustomStepsAdmin_Sytline {

	static WrapperFunctions wrapFunc = new WrapperFunctions();
	static StepBase sb = new StepBase();
	// static StepBase_D2 sb = new StepBase_D2();
	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();
	
	@Given("^I should see value '(.*)' present on page at '(.*)'$")
	public static void i_should_see_value_from_input_field(String expectedText, String element) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					System.out.println("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element),
					Integer.parseInt(System.getProperty("test.implicitlyWait")));
			String actualText = wElement.getAttribute("value");
			WrapperFunctions.highLightElement(wElement);
			expectedText = expectedText.trim();
			actualText = actualText.trim();
			Assert.assertEquals(expectedText, actualText);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}
	
	@When("^I should see the order details '(.*)' feilds present in the page$")
	public static void i_should_see_details_feilds_present_page(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("Order_DateSL");
		CommonSteps.I_should_see_on_page("Status_SL");
		CommonSteps.I_should_see_on_page("Customer_SL");
		CommonSteps.I_should_see_on_page("CustomerPO_SL");
		CommonSteps.I_should_see_on_page("Shipping_TermsSL");
		CommonSteps.I_should_see_on_page("Ship_viaSL");
		CommonSteps.I_should_see_on_page("FromAddress");
		CommonSteps.I_should_see_on_page("ToAddress");
		StepBase.embedScreenshot();
		
	}
	
	@When("^I open form and search '(.*)' & select '(.*)' then I should see '(.*)'$")
	public static void i_open_form_search_filter(String searchText, String selectSearch, String searchResult)
			throws Exception {
		Thread.sleep(5000);
		CommonSteps.I_should_see_on_page("OpenForm");
		CommonSteps.I_focus_click("OpenForm");
		CommonSteps.I_pause_for_seconds(3);
		CommonSteps.I_clear_Field("Filer_SL");
		CommonSteps.I_enter_in_field(searchText, "Filer_SL");
		CommonSteps.I_pause_for_seconds(2);
		Omega_CustomMethods.i_press_enter_key_();
		CommonSteps.I_pause_for_seconds(2);
		CommonSteps.I_should_see_on_page(selectSearch);
		CommonSteps.I_click(selectSearch);
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_click("okButton_SL");
		CommonSteps.I_pause_for_seconds(2);
		CommonSteps.I_should_see_on_page(searchResult);
		StepBase.embedScreenshot();
	}
	
	@When("^I enter '(.*)' from '(.*)' and filter the order$")
	public static void i_enter_ordernumber_filter_the_order(String columnname, String sheetname) throws Exception {
			CommonSteps.I_pause_for_seconds(10);
			String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname,columnname);
			Thread.sleep(3000);
			CommonSteps.I_enter_in_field(fetchValuesFromSpreadSheet, "OrderNumber_SL");
			CommonSteps.I_pause_for_seconds(2);
			CommonSteps.I_should_see_on_page("SearchOrders_SL");
			CommonSteps.I_focus_click("SearchOrders_SL");
			CommonSteps.I_pause_for_seconds(10);
			CommonSteps.I_should_see_on_page("Order_NumberSL");
			i_should_see_value_from_input_field(fetchValuesFromSpreadSheet, "Order_NumberSL");
			StepBase.embedScreenshot();
	}
	
	@When("^I click on Contacts and verify the contact details '(.*)' present in the page$")
	public static void i_click_on_contacts_verify_contactdetails(String sheetname) throws Exception {
		CommonSteps.I_focus_click("Contact_HeadingSL");
		Thread.sleep(5000);
		CommonSteps.I_should_see_on_page("Contacts_InfoSL");
		CommonSteps.I_should_see_on_page("Contacts_EmailSL");
		StepBase.embedScreenshot();
	}
	
	@When("I should see order line item 1 and corresponding details '(.*)' present in the page$")
	public static void i_should_see_orderline_item_1_detials(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("Order_NameSL");
		CommonSteps.I_should_see_on_page("Item_NameSL");
		CommonSteps.I_should_see_on_page("NetPrice_SL");
		CommonSteps.I_should_see_on_page("UnitCost_SL");
		CommonSteps.I_should_see_on_page("AvailableToOrder");
		CommonSteps.I_should_see_on_page("QtyOrdered");
		StepBase.embedScreenshot();
	}
	
	@When("^I click item 2 with order number sheet '(.*)' and column '(.*)'$")
	public static void i_click_item_2(String sheet, String column) throws Exception {
		String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheet, column);
		driver.findElement(By.xpath("(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])[4]")).click();
	}
	
	@When("I should see order line item 2 and corresponding details '(.*)' present in the page$")
	public static void i_should_see_orderline_item_2_detials(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("Order_NameSL");
		CommonSteps.I_should_see_on_page("Item_NameSL");
		CommonSteps.I_should_see_on_page("NetPrice_SL");
		CommonSteps.I_should_see_on_page("UnitCost_SL");
		CommonSteps.I_should_see_on_page("AvailableToOrder");
		CommonSteps.I_should_see_on_page("QtyOrdered");
	}
	
	@Given("^I Login into Sytline DEV$")
	public static void i_login_into_sytline_dev() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		Omega_CustomMethods.i_navigate_to_application("https://omgdev.apptrix.com/WSWebClient/iux.aspx");
		CommonSteps.I_clear_Field("UserName");
		CommonSteps.I_clear_Field("PassWord");
		CommonSteps.I_enter_in_field("tpatel", "UserName");
		CommonSteps.I_enter_in_field("dev1234", "PassWord");
		Thread.sleep(3000);
		CommonSteps.I_clear_Field("Config");
		Thread.sleep(3000);
		CommonSteps.I_enter_in_field("dCT01", "Config");
		Thread.sleep(5000);
		Omega_CustomMethods.i_press_enter_key_();
		Thread.sleep(6000);
		CommonSteps.I_click("Login_SL");
		CommonSteps.I_wait_for_visibility_of_element("OpenForm");
		CommonSteps.I_should_see_on_page("OpenForm");
		StepBase.embedScreenshot();
	}
	
	@Given("^I Logout of Sytline$")
	public void signOutOmegaApplication() throws Exception {
		CommonSteps.I_focus_click("systemButton");
		Thread.sleep(5000);
		CommonSteps.I_mouse_over("FormOption_SL");
		CommonSteps.I_focus_click("SignOut_Option");
		Thread.sleep(5000);
		try {
//		CommonSteps.I_focus_click("SaveCustomerOrder_NoBtn");
		CommonSteps.I_should_see_on_page("UserName");
		}
		catch (Exception e) {
			CommonSteps.I_should_see_on_page("UserName");
		}
	}
	
	@Given("^I Login into Sytline QA$")
	public void signInSytlineQA() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		Omega_CustomMethods.i_navigate_to_application("https://omega.apptrix.com/WSWebClient/Default.aspx");
		CommonSteps.I_clear_Field("UserName");
		CommonSteps.I_clear_Field("PassWord");
		CommonSteps.I_enter_in_field("tpatel", "UserName");
		CommonSteps.I_enter_in_field("pilot123", "PassWord");
		Thread.sleep(3000);
		CommonSteps.I_clear_Field("Config");
		Thread.sleep(3000);
		CommonSteps.I_enter_in_field("pCT01", "Config");
		Thread.sleep(5000);
		Omega_CustomMethods.i_press_enter_key_();
		Thread.sleep(6000);
		CommonSteps.I_click("Login_SL");
		CommonSteps.I_wait_for_visibility_of_element("OpenForm");
		CommonSteps.I_should_see_on_page("OpenForm");
	}
}
