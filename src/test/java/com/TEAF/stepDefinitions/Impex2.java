package com.TEAF.stepDefinitions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.math.RandomUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import com.TEAF.framework.HashMapContainer;

import cucumber.api.java.en.When;

public class Impex2 {

	
	public static void main(String[] args) throws InterruptedException, AWTException {
		impeUrl("HSC1220");
	}
	@When("^I hit impEX applccation to ship the order '(.*)'")
	public static void impeUrl(String ordernumber) throws InterruptedException, AWTException {
		
		String orderNo = HashMapContainer.get(ordernumber);
	//	String orderNo = "HC00539233";
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/src/test/java/com/Resources/chromedriver.exe");

		ChromeDriver idriver = new ChromeDriver();
		idriver.manage().window().maximize();
		Actions ac = new Actions(idriver);

		idriver.get("https://10.47.105.23:9002/hac/login.jsp");
		idriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		idriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		WebElement pass = idriver.findElement(By.name("j_password"));
		pass.sendKeys("nimda");
		idriver.findElement(By.xpath("//button[text()='login']")).click();
		WebElement console = idriver.findElement(By.id("console"));
		ac.moveToElement(console).build().perform();
		idriver.findElement(By.xpath("//a[text()='ImpEx import']")).click();
		Thread.sleep(5000);
		String on =orderNo;
		String in = on.substring(on.length()-5, on.length());

		String text = "INSERT_UPDATE Consignment;code[unique=true];status(code)[default='SHIPPED'];order(code);shippingAddress(PK);warehouse(code);trackingID;carrier\r\n"
				+ ";" + in + ";SHIPPED;" + orderNo + ";8811002920983;MAIN;1Z9Y8" + in+";\r\n"
				+ "INSERT_UPDATE ConsignmentEntry;consignment(code)[unique=true];orderEntry(order(code),entryNumber)[unique=true,cellDecorator=com.lyonscg.dataxfer.dataimport.batch.decorator.OrderEntryDecorator];quantity;shippedQuantity\r\n"
				+ ";" + in + ";" + orderNo + ":1;2.00000000;2.00000000\r\n" +
				";"+ in + ";" + orderNo+ ":2;1.00000000;1.00000000";
		StringSelection stringSelection = new StringSelection(text);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, stringSelection);

		idriver.findElement(By.xpath("//div[@class='CodeMirror-scroll']")).click();
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		Thread.sleep(5000);

		idriver.findElement(By.id("validate")).click();
		Thread.sleep(5000);

		idriver.findElement(By.xpath("//input[@value='Import content']")).click();

		String text2 = idriver.findElement(By.id("msg")).getText();
		if (text2.contains("Successfull")) {
			System.out.println("Pass");
		}
		Thread.sleep(5000);
		idriver.quit();

	}

}
